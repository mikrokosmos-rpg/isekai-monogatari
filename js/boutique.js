let prismicInitialized = false; // Flag to track if Prismic client has been initialized

function loadPrismicModule(callback) {
  console.log('Loading Prismic module...');
  // Use dynamic import to load the prismic module
  import('https://cdn.skypack.dev/@prismicio/client').then((prismic) => {
    console.log('Prismic module loaded successfully');
    // Now that the prismic module is available, you can use it
    if (!prismicInitialized) {
      prismicInitialized = true; // Set the flag to true
      callback(prismic);
    } else {
      console.log('Prismic client already initialized, skipping callback.');
    }
  });
}

async function initializePrismic(prismic) {
  console.log('Initializing Prismic client...');
  const repositoryName = 'isekai-monogatari';
  const client = prismic.createClient(repositoryName);

  if (client) {
    console.log('Prismic client initialized successfully');
  } else {
    console.error('Failed to initialize Prismic client');
  }

  console.log('Fetching Prismic data...');
  const results = await client.getAllByType('boutique', {
    orderings: {
      field: 'my.boutique.item-name',
      direction: 'asc',
    }
  });
  console.log('Prismic data fetched:', results);

  // Sort the results based on the first price value in the prix slice
  results.sort((a, b) => {
    // Assuming prix is an array of objects with an item-variant-price property
    const getPriceFromDocument = (document) => {
      const prixSlice = document.data.body.find((slice) => slice.slice_type === 'autres_prix');
      if (prixSlice && prixSlice.items.length > 0) {
        return prixSlice.items[0]['item-variant-price'] || 0;
      }
      return 0; // Default to 0 if no price is found
    };

    const priceA = getPriceFromDocument(a);
    const priceB = getPriceFromDocument(b);

    return priceA - priceB; // Change to priceB - priceA for descending order
  });

  // Iterate through the results and organize them into respective divs
  results.forEach((document) => {
    const itemType = document.data['item-type'];
    const customId = getTypeCustomId(itemType);

    if (customId) {
      const typeContainer = $(`#${customId}`);
      if (typeContainer.length > 0) {
        const documentElement = createDocumentElement(document);
        typeContainer.append(documentElement);
      }
    }
  });

  console.log('Finished processing Prismic data');
}

function getTypeCustomId(itemType) {
  switch (itemType) {
    case 'Objet usuel':
      return 'usuels';
    case 'Potion':
      return 'potions';
    case 'Technomagie':
      return 'technomagie';
    case 'Artefact':
      return 'artefacts';
    case 'Artisanat ryū':
      return 'ryu';
    default:
      return null;
  }
}
      
function createDocumentElement(document) {
  const listItem = $('<div>').addClass('btq-item');

  // Add a class based on item-type
  if (document.data['item-type'] === "Objet usuel") {
    listItem.addClass('usuel');
  } else if (document.data['item-type'] === "Potion") {
    listItem.addClass('potion');
  } else if (document.data['item-type'] === "Technomagie") {
    listItem.addClass('technomagie');
  } else if (document.data['item-type'] === "Artefact") {
    listItem.addClass('artefact');
  } else if (document.data['item-type'] === "Artisanat ryū") {
    listItem.addClass('ryu');
  }

  const cardContainer = $('<div>').addClass('container');
  const cardCol2 = $('<div>').addClass('col2');

  // Create an element for the item icon
  if (document.data['item-icon'] && document.data['item-icon'].url) {
    const img = $('<img>').addClass('icon');
    img.attr('src', document.data['item-icon'].url);
    cardContainer.append(img);
  } else if (document.data['item-type'] === "Usuel") {
    const iconType = $('<div>').addClass('icon').html('<ion-icon name="gift-outline"></ion-icon>');
    cardContainer.append(iconType);
  } else if (document.data['item-type'] === "Potion") {
    const iconType = $('<div>').addClass('icon').html('<ion-icon name="flask-outline"></ion-icon>');
    cardContainer.append(iconType);
  } else if (document.data['item-type'] === "Technomagie") {
    const iconType = $('<div>').addClass('icon').html('<ion-icon name="qr-code-outline"></ion-icon>');
    cardContainer.append(iconType);
  } else if (document.data['item-type'] === "Artefact") {
    const iconType = $('<div>').addClass('icon').html('<ion-icon name="diamond-outline"></ion-icon>');
    cardContainer.append(iconType);
  } else if (document.data['item-type'] === "Artisanat ryū") {
    const iconType = $('<div>').addClass('icon').html('<ion-icon name="leaf-outline"></ion-icon>');
    cardContainer.append(iconType);
  }

  // Create elements for item name and description
  if (document.data['item-type']) {
    const cardType = $('<span>').addClass('type').text(document.data['item-type']);
    cardCol2.append(cardType);
  }
  const cardTitle = $('<div>').addClass('title');

  if (document.data['item-name']) {
    const cardName = $('<span>').addClass('nom').text(document.data['item-name']);
    cardTitle.append(cardName);
  }
  if (document.data['item-desc-short']) {
    const cardSub = $('<span>').addClass('subtitle').text(document.data['item-desc-short']);
    cardTitle.append(cardSub);
  }
  cardCol2.append(cardTitle);

  if (document.data.body) {
    const autresPrixSlice = document.data.body.find((slice) => slice.slice_type === 'autres_prix');
    if (autresPrixSlice) {
      const autresPrixContainer = $('<div>').addClass('autres-prix');
      autresPrixSlice.items.forEach((item) => {
        const variant = item['item-variant'];
        const price = item['item-variant-price'];
        if (variant && price) {
          const priceElement = $('<div>').addClass('other-price');
          priceElement.append($('<span>').addClass('variant').text(variant));
          priceElement.append($('<span>').addClass('price').text(price + '玉'));
          autresPrixContainer.append(priceElement);
        } else if (variant && !price) {
          const priceElement = $('<div>').addClass('other-price');
          priceElement.append($('<span>').addClass('variant').text(variant));
          priceElement.append($('<span>').addClass('price').text('Prix Libre'));
          autresPrixContainer.append(priceElement);
        }
      });
      cardCol2.append(autresPrixContainer);
    }
  }

  cardContainer.append(cardCol2);

  
  if (document.data['item-desc-long']) {
  const descSpan = $('<div>').addClass('desc-container');
  const description = $('<div>').addClass('description');
  document.data['item-desc-long'].forEach((information) => {
    if (information.type === 'heading6') {
      // Handle h6 elements
      const h6Element = $('<h6>').text(information.text);
      description.append(h6Element);
    } else if (information.type === 'list-item') {
      // Handle list items (ul)
      const listItemElement = $('<li>');

      const spans = information.spans;

      // Loop through spans
      for (let i = 0; i < spans.length; i++) {
        const { start, end, type } = spans[i];
        const spanText = information.text.substring(start, end);
        const nextSpan = spans[i + 1];

        if (type === 'strong') {
          // Handle strong text
          const strongElement = $('<strong>').text(spanText);
          listItemElement.append(strongElement);

          // Check if the next span is not strong, and if not, append the following text
          if (!nextSpan || nextSpan.type !== 'strong') {
            listItemElement.append(information.text.substring(end, nextSpan ? nextSpan.start : undefined));
          }
        } else if (type === 'em') {
          // Handle em text
          const emElement = $('<em>').text(spanText);
          listItemElement.append(emElement);

          // Check if the next span is not em, and if not, append the following text
          if (!nextSpan || nextSpan.type !== 'em') {
            listItemElement.append(information.text.substring(end, nextSpan ? nextSpan.start : undefined));
          }
        } else if (type === 'hyperlink') {
          const aElement = $('<a>').attr('href', span.data.url).attr('target', '_blank').text(spanText);
          listItemElement.append(aElement);
        }
      }

      description.append(listItemElement);
    } else {
      const paragraphElement = $('<p>').text(information.text);

      information.spans.forEach((span) => {
        const { start, end, type } = span;
        const formattedText = information.text.substring(start, end);
        if (type === 'strong') {
          const strongElement = $('<strong>').text(formattedText);
          paragraphElement.html((i, html) => {
            return html.replace(formattedText, strongElement[0].outerHTML);
          });
        } else if (type === 'em') {
          const emElement = $('<em>').text(formattedText);
          paragraphElement.html((i, html) => {
            return html.replace(formattedText, emElement[0].outerHTML);
          });
        } else if (type === 'hyperlink') {
          const aElement = $('<a>').attr('href', span.data.url).attr('target', '_blank').text(formattedText);
          paragraphElement.html((i, html) => {
            return html.replace(formattedText, aElement[0].outerHTML);
          });
        }
      });

      description.append(paragraphElement);
    }
  });

  descSpan.append(description);
  cardContainer.append(descSpan);
}

  // Add the item to the grid
  listItem.append(cardContainer);
  return listItem;
}

document.addEventListener('DOMContentLoaded', function () {
  console.log('DOMContentLoaded event fired');
  if (window.location.href.includes('t2468-boutique-achats-reclamations')) {
    console.log('Condition met, executing script...');
    // Load the Prismic module dynamically, and then initialize the Prismic client
    loadPrismicModule(initializePrismic);
  }
});
