$(document).ready(function() {
    // Call pour la base de données
    const forumathonUrl = 'https://zxzancdbygtprcouqlim.supabase.co';
    const forumathonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Inp4emFuY2RieWd0cHJjb3VxbGltIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA1OTEzMzMsImV4cCI6MjAyNjE2NzMzM30.yh0Pbug8hettMgTXw1OjWCHv_IwbCrQMKNMs1JPQPQc';
    const { createClient } = supabase;
    const forumathonClient = createClient(forumathonUrl, forumathonKey);

    // Variables initiales de goal et de palier
    var goal = 5000;
    var level = 1;

    // Affiche le form pour ajouter des mots uniquement pour les membres enregistrés
    jQuery(function($) {
        if (_userdata["user_id"] > (0)){
            $('#addBtn').css("display", "inline-block");
        }
      });

    // MAJ de la progression
    async function updateProgress() {
        try {
            const response = await forumathonClient.from('Forumathon_032024').select('mots, maison');
            const data = response.data;
            
            if (response.error) {
                console.error('Error fetching data from Supabase:', response.error.message);
                return;
            }

            let sum = 0;
            const maisonTotals = {
                "Dojin": 0,
                "Fugi": 0,
                "Hiisa": 0,
                "Raiyo": 0,
                "Suisei": 0
            };

            // Update la somme globale et la somme des mots pour chaque maison
            data.forEach(item => {
                sum += item.mots;
                if (maisonTotals.hasOwnProperty(item.maison)) {
                    maisonTotals[item.maison] += item.mots;
                }
            });

            // Calcule le % du goal global, le countdown vers le prochain goal et incrémente les paliers
            let globalPercentage = (sum / goal) * 100;
            let wordsToGoal = goal - sum;
            let nextLevel = level + 1;

            $("#progress").text(`${wordsToGoal} mots`);
            $("#nextLevel").text(`palier ${nextLevel}`);
            $("#wordCount").text(`${sum}/${goal} mots`);
            $("#progress-bar").css("width", `${globalPercentage}%`);
            $("#level").text(`Palier ${level}`);

            // Incrémentation des goals et des paliers si la somme des mots est supérieure au goal initial
            while (sum >= goal) {
                level++;
                goal += 5000;
                wordsToGoal = goal - sum;
                $("#progress").text(`${wordsToGoal} mots`);
                $("#nextLevel").text(`palier ${level + 1}`);
                $("#wordCount").text(`${sum}/${goal} mots`);
                $("#level").text(`Palier ${level}`);

                const segmentSize = 5000;
                let segmentProgress = sum - (goal - segmentSize);
                let segmentPercentage = (segmentProgress / segmentSize) * 100;
                $("#progress-bar").css("width", `${segmentPercentage}%`);
            }

            // Affiche les compteurs par maison liés à chaque somme de maison
            const maisons = [
                { id: "Dojin", bar: "#dojinProgressBar", count: "#dojinWordCount" },
                { id: "Fugi", bar: "#fugiProgressBar", count: "#fugiWordCount" },
                { id: "Hiisa", bar: "#hiisaProgressBar", count: "#hiisaWordCount" },
                { id: "Raiyo", bar: "#raiyoProgressBar", count: "#raiyoWordCount" },
                { id: "Suisei", bar: "#suiseiProgressBar", count: "#suiseiWordCount" }
            ];
            maisons.forEach(({ id, bar, count }) => {
                const totalMots = maisonTotals[id];
                const maisonGoal = goal;
                const maisonPercentage = (totalMots / maisonGoal) * 100;
                $(count).text(`${totalMots} mots`);
                $(bar).css("width", `${maisonPercentage}%`);
            });

        } catch (error) {
            console.error('Error fetching data from Supabase:', error.message);
        }
    }

    // Style du form pour ajouter les mots (afficher/cacher)
    $("#addBtn").click(function(){
        $("#addModal").css("display", "block");
        $("#addBtn").css("display", "none");
    });
    $(".close").click(function(){
        $("#addModal").css("display", "none");
        $("#addBtn").css("display", "inline-block");
    });
    $(window).click(function(event) {
        if (event.target == $("#addModal")[0]) {
            $("#addModal").css("display", "none");
            $("#addBtn").css("display", "inline-block");
        }
    });

    // Envoi d'un nouveau compte de mots à la base de données
    $("#confirmBtn").click(function() {
        var motsValue = parseInt($("#goalInput").val());
        var maisonValue = $("#maison").val();

        // Check si le field Mots et le field Maison sont remplis avant d'envoyer
        if (!isNaN(motsValue) && maisonValue.trim() !== '') {
            forumathonClient.from('Forumathon_032024')
            .insert([{ mots: motsValue, maison: maisonValue }])
            .then(response => {
                updateProgress();
                $("#addModal").css("display", "none");
                $("#addBtn").css("display", "inline-block");
            })
            .catch(error => {
                console.error('Error adding new entry to Supabase:', error.message);
            });
        } else {
            alert("Oups, ça n'a pas marché ! Vérifie que tu as bien rempli tous les champs et recommence.")
        }
    });
    
    // Initialise les infos du compteur au chargement de la page
    updateProgress();

    // Logique du collapse/expand pour les Compteurs par Maison
    const toggleButton = $('#forumathon-maisons button');
    const toggleIcon = toggleButton.find('ion-icon');
    const progressBarsContainer = $('#forumathon-maisons div.progress');
    function toggleProgressBars() {
        const isVisible = progressBarsContainer.is(':visible');
        if (isVisible) {
            progressBarsContainer.hide();
            toggleIcon.attr('name', 'chevron-down-outline');
        } else {
            progressBarsContainer.show();
            toggleIcon.attr('name', 'chevron-up-outline');
        }
    }
    toggleButton.on('click', toggleProgressBars);
    
});
