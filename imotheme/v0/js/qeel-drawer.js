// Appel à la base de données Listings (peut être optionnel si déjà appelé dans le footer du forum ?)
// const supabaseUrl = 'https://gbejdguvqhsdvwtmqfbp.supabase.co';
// const supabaseAnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdiZWpkZ3V2cWhzZHZ3dG1xZmJwIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA0NjUxOTksImV4cCI6MjAyNjA0MTE5OX0.kK8RMRPvXehp86KIM-sXfq9qERgaSLEFOqXuwZVZUqw';
// const { createClient } = supabase;

$(document).ready(function () {
    var drawerContainer = $('#qeel_drawer_container');
    var drawer = $('#qeel_drawer');
    var button = $('#qeel_button');

    // Initialiser le conteneur du drawer
    if (!drawer.length) {
        drawer = $('<div id="qeel_drawer"></div>');
        drawerContainer.append(drawer);
        drawer.css({
            display: 'grid',
            right: '-400px'
        });
    }

    // Event listener sur le bouton du QEEL
    button.on('click', toggleDrawer);

    // Fonction pour toggle le drawer
    function toggleDrawer() {
        var isDrawerVisible = drawer.css('right') === '0px';
        drawer.css('right', isDrawerVisible ? '-400px' : '0px');
        button.toggleClass('open', !isDrawerVisible);

        // Ne remplit le drawer que s'il est visible
        if (!isDrawerVisible) {
            populateDrawerContent();
        }
    }

    // Fonction pour remplir le drawer
    function populateDrawerContent() {
        // Base de la structure du contenu
        drawerContent = `
            <div>
                <h3>Actuellement connecté.es&nbsp;—&nbsp;<span id="online_number"></span></h3>
                <div id="qeel_online" class="loading"></div>
            </div>
            <div>
                <h3>Dernières 48h</h3>
                <div id="qeel_48h" class="loading"></div>
            </div>
        `;
        drawer.html(drawerContent);
        // Ajoute le nombre de personnes connectées dans le titre
        $('#online_number').load('/ #user_number > strong:first-of-type');

        // Fonctions pour charger le contenu de chaque section
        loadOnlineContent();
        loadRecentContent();
    }

    // Charger la liste des membres en ligne
    function loadOnlineContent() {
        $('#qeel_online').load('/ div#loggedin', function () {

            // Retire le texte par défaut sur FA
            var pifContents = $('#qeel_online div#loggedin').contents();
            pifContents.each(function () {
                if (this.nodeType === 3) {
                    this.nodeValue = this.nodeValue.replace(/Utilisateurs enregistrés : Aucun/g, 'Aucun membre connecté');
                    this.nodeValue = this.nodeValue.replace(/Utilisateurs enregistrés : /g, '');
                    this.nodeValue = this.nodeValue.replace(/, /g, '');
                }
            });

            // Loope la fonction pour charger les avatars et les statuts pour chaque lien (membre) dans la liste
            var userLinksOnline = $('#qeel_online div#loggedin a');
            userLinksOnline.each(function () {
                var userLink = $(this);
                getAvatarUrl(userLink, function (avatarUrl) {
                    loadUserStatus(userLink, avatarUrl);
                });
            });

            // Timeout de chargement pour le skeleton
            setTimeout(function () {
                $('#qeel_online').removeClass('loading');
            }, 2000);
        });
    }

    // Charger la liste des membres connectés pendant les dernières 48h
    function loadRecentContent() {
        $('#qeel_48h').load('/ div#latest', function () {

            // Retire le texte par défaut sur FA
            var kaboumContents = $('#qeel_48h div#latest').contents();
            kaboumContents.each(function () {
                if (this.nodeType === 3) {
                    this.nodeValue = this.nodeValue.replace(/Membres connectés au cours des 48 dernières heures : /g, '');
                    this.nodeValue = this.nodeValue.replace(/, /g, '');
                }
            });
            
            // Loope la fonction pour charger les avatars et les statuts pour chaque lien (membre) dans la liste
            var userLinks48h = $('#qeel_48h div#latest a');
            userLinks48h.each(function () {
                var userLink = $(this);
                getAvatarUrl(userLink, function (avatarUrl) {
                    loadUserStatus(userLink, avatarUrl);
                });
            });

            // Timeout de chargement pour le skeleton
            setTimeout(function () {
                $('#qeel_48h').removeClass('loading');

                // Loope la liste des membres connectés et retire les membres en ligne de la liste des 48h pour éviter les doublons
                var userLinksPif = $('#qeel_online div#loggedin a');
                userLinksPif.each(function () {
                    var usernamePif = $(this).text().trim();
                    $('#qeel_48h div#latest a:contains("' + usernamePif + '")').remove();
                });
                
                // Placeholder si la liste est vide
                if ($('#qeel_48h div#latest a').length === 0) {
                    $('#qeel_48h div#latest').text('Il semblerait que tout le monde soit en ligne !');
                }

            }, 2000);
        });
    }

    // Fonction pour créer la ligne par membre connecté
    function addStatusAndAvatar(userLink, statusClass, avatarUrl, hoverText, isStaff) {
        // Récupère le username FA avec la couleur de groupe
        var usernameElement = userLink.find('span[style^="color"], span[class^="style"]');
        // Trim pour ne garder que le nom sans la string HTML
        var username = usernameElement.length ? usernameElement.text().trim() : userLink.text().trim();

        // Ajoute l'avatar et la classe statut depuis le fetch DB
        var avatarImg = $('<img>').attr('data-src', avatarUrl).addClass('avatar_qeel lazyload');
        var statusSpan = $('<span></span>').addClass('status ' + statusClass);

        userLink.append(avatarImg, statusSpan);
        
        // Si staff, ajoute le tag
        if (isStaff) {
            userLink.append('<span class="tag">staff</span>');
        }

        // Wrap le tout dans un span pour inclure le username FA
        userLink.contents().filter(function () {
            return this.nodeType === 3;
        }).wrap('<span class="username"></span>');
    }

    // Fonction pour récupérer les statuts de présence
    function loadUserStatus(userLink, avatarUrl) {
        var userId = getUserIdFromHref(userLink.attr('href'));
        const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);
 
        // Fetch les données de la table Joueurs dans la database
        supabaseClient
            .from('Joueurs')
            .select('*')
            .contains('comptes', `"${userId}"`)
            .then(({ data, error }) => {
                if (error) {
                    console.error('Error fetching data from Joueurs:', error.message);
                    alert('Failed to fetch data from Joueurs.');
                    return;
                }

                if (data && data.length > 0) {
                    var userData = data[0];
                    var trimmedHtml = userData.statutPres.trim();
                    var statusClass = '';

                    // Logique des statuts
                    if (trimmedHtml === 'Présent') {
                        statusClass = 'present';
                    } else if (trimmedHtml === 'Présence réduite') {
                        statusClass = 'presred';
                    } else if (trimmedHtml === 'Absent') {
                        statusClass = 'absent';
                    }

                    // Passe la prop Staff si le boolean isStaff est true dans la database
                    var isStaff = userData.isStaff;

                    // Trigger la fonction pour récupérer les avatars
                    addStatusAndAvatar(userLink, statusClass, avatarUrl, trimmedHtml, isStaff);
                } else {
                    // Fallback si le joueur n'existe pas
                    addStatusAndAvatar(userLink, 'none', avatarUrl, '');
                }
            });
    }

    // Fonction pour récupérer chaque userID depuis l'URL du lien sur le nom
    function getUserIdFromHref(href) {
        var matches = href.match(/\/u(\d+)/);
        return matches ? matches[1] : null;
    }

     // Fonction pour récupérer les avatars depuis la database
    function getAvatarUrl(userLink, callback) {
        // Récupère le userID
        var userId = getUserIdFromHref(userLink.attr('href'));
        
        // Récupère le champ image de la table Registre pour chaque userID
        const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);
        supabaseClient
            .from('Registre')
            .select('image')
            .eq('id', userId)
            .then(({ data, error }) => {
                if (error) {
                    console.error('Error fetching data from Registre:', error.message);
                    fetchAvatarFromUrl();
                    return;
                }
                if (data && data.length > 0) {
                    var avatarUrl = data[0].image;
                    callback(avatarUrl);
                } else {
                    // Fallback qui récupère l'avatar sur le profil FA si le personnage n'est pas dans le Registre
                    fetchAvatarFromUrl();
                }
            });

        // Fonction du fallback pour l'avatar
        function fetchAvatarFromUrl() {
            var userPageUrl = '/u' + userId;
            $.ajax({
                url: userPageUrl,
                method: 'GET',
                success: function(html) {
                    var avatarUrl = $(html).find('#avQEEL img').attr('src');
                    callback(avatarUrl);
                },
                error: function(error) {
                    console.error('Error fetching user avatar:', error);
                    callback(null);
                }
            });
        }
    }
});