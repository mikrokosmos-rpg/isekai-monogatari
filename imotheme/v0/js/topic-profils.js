$(document).ready(function() {
  // Appel à la database
  // const supabaseUrl = 'https://gbejdguvqhsdvwtmqfbp.supabase.co';
  // const supabaseAnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdiZWpkZ3V2cWhzZHZ3dG1xZmJwIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA0NjUxOTksImV4cCI6MjAyNjA0MTE5OX0.kK8RMRPvXehp86KIM-sXfq9qERgaSLEFOqXuwZVZUqw';
  // const { createClient } = supabase;
    const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);
  
    // Fonction qui loop pour chaque post dans la page
    $('.post').each(function() {
      var postElement = $(this);
      // Récupère le userID du post
      var userID = postElement.find('.postprofile-avatar').attr('data-id');
  
      // Ajoute l'icône image pour le mode Lecture dans la bonne div
      var iconContainer = postElement.find('dt .icone');
      postElement.find('.postprofile-info .champ_').each(function() {
          var iconImg = $(this).clone();
          iconContainer.append(iconImg);
      });
  
      // Fetch le faceclaim du personnage dans la table Registre
      supabaseClient.from('Registre').select('*').eq('id', userID).then(({ data, error }) => {
        if (error) {
          console.error('Error fetching data from Registre:', error.message);
          return;
        }
        if (data && data.length > 0) {
          var faceclaim = data[0].faceclaim;
          postElement.find('.profile-hover > .faceclaim > span').text(faceclaim);
        } else {
          // Fallback si le membre ne figure pas au registre pour récupérer le champ du profil FA à la place 
          cloneContent(postElement, '.postprofile-info .champ_faceclaim > span:not(.label)', '.profile-hover > .faceclaim > span');
        }
      });

      // Définit les div de destination pour le statut de présence
      var statutContainer1 = postElement.find('.profile-hover > .tags > .statut');
      var statutContainer2 = postElement.find('.postprofile-name > .statut');
  
      // Fetch les informations du joueur dans la table Joueurs
      supabaseClient.from('Joueurs').select('*').contains('comptes', `"${userID}"`).then(({ data, error }) => {
        if (error) {
          console.error('Error fetching data from Registre:', error.message);
          return;
        }
        if (data && data.length > 0) {
          // Récupère le pseudo, les pronoms et le statut de présence
          var pseudo = data[0].pseudo;
          var pronoms = data[0].pronoms;
          var statutPres = data[0].statutPres;

          // Récupère les multicomptes
          let comptes = data[0].comptes;
          // Exclut le userID actuel de la liste des multicomptes
          comptes = comptes.filter(function (compte) {
            return compte !== userID.toString();
          });
          // Si le membre n'a qu'un personnage, hide la div multicomptes pour le post
          if (comptes.length === 0) {
            postElement.find('div.multicomptes').hide();
          } else {
            postElement.find('div.multicomptes').show();
          }
    
          // Remplit les div avec le pseudo et les pronoms récupérés dans la database
          postElement.find('.profile-hover > .irl > .pseudo-irl').text(pseudo);
          postElement.find('.profile-hover > .irl > .pronoms').text(pronoms);
          // Logique pour remplir les divs de statut de présence (mode normal et mode lecture)
          if (statutPres === 'Présent.e') {
            statutContainer1.addClass('pres');
            statutContainer2.addClass('pres');
            statutContainer1.append("Présent.e");
            statutContainer2.append("Présent.e");
          } else if (statutPres === 'Présence réduite') {
            statutContainer1.addClass('presred');
            statutContainer1.append("Présence réduite");
            statutContainer2.addClass('presred');
            statutContainer2.append("Présence réduite");
          } else if (statutPres === 'Absent.e') {
            statutContainer1.addClass('abs');
            statutContainer1.append("Absent.e");
            statutContainer2.addClass('abs');
            statutContainer2.append("Absent.e");
          }
        } else {
          // Fallback si le joueur n'existe pas dans la database
          console.log('Ce joueur ne figure pas dans la base de données.');
          // Récupère le contenu des champs de profil FA à la place
          cloneContent(postElement, '.postprofile-info .champ_pseudo-irl > span:not(.label)', '.profile-hover > .irl > .pseudo-irl');
          cloneContent(postElement, '.postprofile-info .champ_pronoms-irl > span:not(.label)', '.profile-hover > .irl > .pronoms');
          // Ajoute le statut de présence par défaut
          statutContainer1.addClass('pres');
          statutContainer2.addClass('pres');
          statutContainer1.append("Présent.e");
          statutContainer2.append("Présent.e");
        }
      });
  
      // Définit le conteneur de la div multicomptes
      var profileLinksContainer = postElement.find('div.multicomptes > span');
      // Définit un statut fermé par défaut
      var isBoxOpen = false;
      
      // Logique pour ouvrir + remplir la div multicomptes si il y en a
      postElement.find('.multicomptes > a').click(function () {
        postElement.find('.multicomptes').toggleClass('open');
        if (!isBoxOpen) {
          fetchMulticomptes();
        } else {
          profileLinksContainer.empty();
        }
        isBoxOpen = !isBoxOpen;
      });
      
      // Fonction fetch des multicomptes
      function fetchMulticomptes() {
        // Définit la div de destination
        var profileLinksContainer = postElement.find('div.multicomptes > span');
        const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);
        // Définit le userID du compte
        var userID = postElement.find('.postprofile-avatar').attr('data-id');

        // Fetch les infos du joueur dans la table Joueurs en fonction du userID de son compte
        supabaseClient.from('Joueurs').select('*').contains('comptes', `"${userID}"`).then(({ data, error }) => {
          if (error) {
              console.error('Error fetching data from Joueurs:', error.message);
              displayEmptyFallback();
              return;
          }
          if (data && data.length > 0) {
            let comptes = data[0].comptes;
            // Exclut le userID actuel de la liste des multicomptes
            comptes = comptes.filter(function (compte) {
              return compte !== userID.toString();
            });
            // Si le joueur n'a qu'un compte, utiliser le fallback
            if (comptes.length === 0) {
              displayEmptyFallback();
            } else {
              // Loop l'array multicomptes si il y en a
              Promise.all(comptes.map(fetchAccountData)).then(function (links) {
                links.filter(link => link !== null).forEach(link => {
                    profileLinksContainer.append(link);
                });
              });
            }
          } else {
            displayEmptyFallback();
          }
        });

        // Fonction de loop pour récupérer les infos de chaque multicompte dans l'array
        function fetchAccountData(accountID) {
          return new Promise(function (resolve) {
            // Fetch dans la table Registre en fonction du accountID du multicompte
            supabaseClient.from('Registre').select('*').eq('id', accountID).then(({ data, error }) => {
              if (error) {
                  console.error('Error fetching data from Registre:', error.message);
                  resolve(null);
                  return;
              }
              if (data && data.length > 0) {
                // Si trouvé dans la database, crée les icônes avec l'image du Registre
                var accountData = data[0];
                var userUrl = '/u' + accountID;    
                var profileLink = $('<a>').attr('href', userUrl);
                var profileImage = $('<img>').attr('src', accountData.image).attr('alt', accountData.nom);
                var usernameSpan = $('<span>').addClass("nom").append(accountData.nom);

                profileLink.append(profileImage, usernameSpan);

                resolve(profileLink);
              } else {
                // NOTE POUR PLUS TARD : 
                // si des membres essaient de lister des comptes pas encore dans le registre, need un fallback ici et dans le gestionnaire des multicomptes avec juste l'ID, pas de nom et un cercle gris sans image ?
                resolve(null);
              }
            });
          });
        }

        // Fallback si pas de multicompte pour planquer la div de destination
        function displayEmptyFallback() {
          postElement.find('div.multicomptes').hide();
        }
      }
  
      // Logique pour ajouter l'icône personnalisée depuis les champs de profil FA
      postElement.find('.postprofile-info .champ_symbole > span:not(.label)').each(function() {
          var iconType = postElement.find('.postprofile-info .champ_type-de-symbole > span:nth-child(2)').text().trim();
          var iconName = $(this).text().trim();
          var profileHover = postElement.find('.profile-hover');
          if (iconType === 'Ionicons') {
              var newIcon = $('<ion-icon></ion-icon>').attr('name', iconName);
              profileHover.append(newIcon);
          } else if (iconType === 'Feather') {
              var newIcon = $('<i></i>').attr('data-feather', iconName);
              profileHover.append(newIcon);
          } else if (iconType === 'Bootstrap') {
              var newIcon = $('<i></i>').attr('class', 'bi-' + iconName);
              profileHover.append(newIcon);
          } else if (iconType === 'Iconify') {
              var newIcon = $('<iconify-icon></iconify-icon>').attr('icon', iconName);
              profileHover.append(newIcon);
          }
      });
      
      // Clone le contenu des champs de profil FA dans les bonnes div de destination
      cloneContent(postElement, '.postprofile-info .champ_cr__00e9dits > span:not(.label)', '.profile-hover > .credits > span');
      cloneContent(postElement, '.postprofile-info .champ_messages > span:not(.label)', '.profile-stats > .messages > span');
      cloneContent(postElement, '.postprofile-info .champ_points-rp > span:not(.label)', '.profile-stats > .points > span');
      cloneContent(postElement, '.postprofile-info .champ_aesthetic-irp > span:not(.label)', 'div.aesthetic > span');
  
      // Logique pour afficher la couleur de dialogue et la styler
      postElement.find('.couleur-dia').each(function() {
        var colorContainer = $(this);
        var couleurSpan = postElement.find('.postprofile-info .champ_couleur-de-dialogues > span:not(.label)');
        var couleurWrite = couleurSpan.text().trim();
        var clonedContent = couleurSpan.clone();
        colorContainer.append(clonedContent);
        if (!couleurWrite || couleurWrite === '&nbsp;' || couleurWrite === '-') {
          colorContainer[0].style.setProperty('--co3', 'var(--co3)');
        } else {
          colorContainer[0].style.setProperty('--co3', couleurWrite);
        }
      });
  
    // Fonction utilitaire de clonage de contenu
    function cloneContent(postElement, sourceSelector, targetSelector) {
      postElement.find(sourceSelector).each(function() {
        var sourceField = $(this);
        var targetContainer = postElement.find(targetSelector);
        var clonedContent = sourceField.clone();
        targetContainer.append(clonedContent);
      });
    }
  });
});