document.addEventListener("DOMContentLoaded", function() {

  // Call à la base de données Listings
  // const supabaseUrl = 'https://gbejdguvqhsdvwtmqfbp.supabase.co';
  // const supabaseAnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdiZWpkZ3V2cWhzZHZ3dG1xZmJwIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA0NjUxOTksImV4cCI6MjAyNjA0MTE5OX0.kK8RMRPvXehp86KIM-sXfq9qERgaSLEFOqXuwZVZUqw';
  const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);

  var profilElement = $('#regular');
  var statutContainer = profilElement.find('div.statut');
  var mpContainer = profilElement.find('div.mp');
  var discordContainer = profilElement.find('div.discord');

  // Récupère le userID du profil
  var dataUserId = jQuery('#regular').attr('data-user-id');
  var userID = dataUserId ? dataUserId.replace('u', '') : null;

  // Fetch le faceclaim du personnage dans la table Registre
  supabaseClient.from('Registre').select('*').eq('id', userID).then(({ data, error }) => {
    if (error) {
      console.error('Error fetching data from Registre:', error.message);
      return;
    }
    if (data && data.length > 0) {
      var faceclaim = data[0].faceclaim;
      jQuery('#regular').find('.faceclaim > span').text(faceclaim);
    } else {
      cloneContent(profilElement, '#field_id-11 div.field_uneditable', '.faceclaim > span');
    }
  });

  // Fetch les infos du joueur dans la table Joueurs
  supabaseClient.from('Joueurs').select('*').contains('comptes', `"${userID}"`).then(({ data, error }) => {
    if (error) {
      console.error('Error fetching data from Registre:', error.message);
      return;
    }
    if (data && data.length > 0) {
      var pseudo = data[0].pseudo;
      var pronoms = data[0].pronoms;
      var statutPres = data[0].statutPres;
      var mpOK = data[0].mpOK;
      var discordOK = data[0].discordOK;

      // Ajoute le pseudo et les pronoms
      document.querySelector('#pseudo').textContent = pseudo;
      document.querySelector('#pronoms').textContent = pronoms;

      // Logique du statut de présence
      if (statutPres === 'Présent.e') {
        statutContainer.addClass('pres');
        statutContainer.append("Présent.e");
      } else if (statutPres === 'Présence réduite') {
        statutContainer.addClass('presred');
        statutContainer.append("Présence réduite");
      } else if (statutPres === 'Absent.e') {
        statutContainer.addClass('abs');
        statutContainer.append("Absent.e");
      }

      // Logique des champs de contact
      if (mpOK === false) {
        mpContainer.addClass('closed');
        var closedIcon = $('<ion-icon name="close-circle-outline"></ion-icon>');
        mpContainer.append(closedIcon);
      } else {
        mpContainer.addClass('open');
        var openIcon = $('<ion-icon name="checkmark-circle-outline"></ion-icon>');
        mpContainer.append(openIcon);
      }
      if (discordOK === false) {
        discordContainer.addClass('closed');
        var closedIcon = $('<ion-icon name="close-circle-outline"></ion-icon>');
        discordContainer.append(closedIcon);
      } else {
        discordContainer.addClass('open');
        var openIcon = $('<ion-icon name="checkmark-circle-outline"></ion-icon>');
        discordContainer.append(openIcon);
      }
    } else {
      // Fallback si le joueur n'est pas enregistré
      console.log("Ce.tte joueur.se n'est pas enregistré.e dans la base de donnés.");

      // Récupère le pseudo et les pronoms du profil Forumactif
      cloneContent(profilElement, '#field_id-8 div.field_uneditable', '#pseudo');
      cloneContent(profilElement, '#field_id26 div.field_uneditable', '#pronoms');

      // Ajoute le statut de présence et les champs de contact par défaut
      statutContainer.addClass('pres');
      statutContainer.append("Présent.e");
      mpContainer.addClass('open');
      var openIcon = $('<ion-icon name="checkmark-circle-outline"></ion-icon>');
      mpContainer.append(openIcon);
      discordContainer.addClass('open');
      var openIcon = $('<ion-icon name="checkmark-circle-outline"></ion-icon>');
      discordContainer.append(openIcon);
    }
  });

  // Affichage de l'icône personnalisée depuis le profil Forumactif
  profilElement.find('#field_id30 div.field_uneditable').each(function() {
    var iconType = profilElement.find('#field_id29 div.field_uneditable').text().trim();
    var iconName = $(this).text().trim();
    if (iconType === 'Ionicons') {
      var newIcon = $('<ion-icon></ion-icon>').attr('name', iconName);
      profilElement.find('.profil-name-title').append(newIcon);
    } else if (iconType === 'Feather') {
      var newIcon = $('<i></i>').attr('data-feather', iconName);
      profilElement.find('.profil-name-title').append(newIcon);
    } else if (iconType === 'Bootstrap') {
      var newIcon = $('<i></i>').attr('class', 'bi-' + iconName);
      profilElement.find('.profil-name-title').append(newIcon);
    } else if (iconType === 'Iconify') {
      var newIcon = $('<iconify-icon></iconify-icon>').attr('icon', iconName);
      profilElement.find('.profil-name-title').append(newIcon);
    }
  });

  // Ajout du bouton Modifier le profil pour les admins
  var adminLink = profilElement.find('span#admin_link > a');
  var adminText = adminLink.text().trim();
  if (adminLink.length > 0) {
    adminLink.empty().text('Modifier le profil');
  } 

  // Clone le reste des champs Forumactif dans les bonnes divs du profil
  cloneContent(profilElement, '#field_id-6 div.field_uneditable', 'div.profil-msg > dd');
  cloneContent(profilElement, '#field_id-13 div.field_uneditable', 'div.profil-points > dd');
  cloneContent(profilElement, '#field_id12 div.field_uneditable', 'div.profil-dama > dd');
  cloneContent(profilElement, '#field_id10 div.field_uneditable', '#content_tab2 > span');
  cloneContent(profilElement, '#field_id27 div.field_uneditable', '.credits > span');
  fetchMulticomptes();

  // Logique de la couleur de dialogue en fonction des champs Forumactif
  var couleurSpan = profilElement.find('#field_id7 div.field_uneditable');
  var couleurWrite = couleurSpan.text().trim();
  var colorContainer = profilElement.find('span.couleur-dia');
  var colorText = colorContainer.find('span');
  colorText.append(couleurWrite);
  if (!couleurWrite || couleurWrite === '&nbsp;' || couleurWrite === '-') {
    colorContainer[0].style.setProperty('--co3', 'var(--co3)');
  } else {
    colorContainer[0].style.setProperty('--co3', couleurWrite);
  }
    
  // Fonction Cloner
  function cloneContent(profilElement, sourceSelector, targetSelector) {
    profilElement.find(sourceSelector).each(function() {
      var sourceField = $(this);
      var targetContainer = profilElement.find(targetSelector);
      var clonedContent = sourceField.clone();
      targetContainer.append(clonedContent);
    });
  }

  // Fonction Fetch des Multicomptes
  function fetchMulticomptes() {
    // Définiton de la div Multicomptes de destination
    var profileLinksContainer = jQuery('#regular').find('div.multicomptes > div.multicomptes-list > span');
    // Re-définition du client Supabase au cas où
    const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);

    // Récupère le userID du profil actuel
    var dataUserId = jQuery('#regular').attr('data-user-id');
    var userID = dataUserId ? dataUserId.replace('u', '') : null;

      // Fetch des multicomptes dans la table Joueurs de la base de données
      supabaseClient.from('Joueurs').select('*').contains('comptes', `"${userID}"`).then(({ data, error }) => {
        if (error) {
            console.error('Error fetching data from Joueurs:', error.message);
            displayEmptyFallback();
            return;
        }

        // Si le Joueur a des multicomptes
        if (data && data.length > 0) {
          let comptes = data[0].comptes;
          // Retire le compte userID actuel de l'array Multicomptes
          comptes = comptes.filter(function (compte) {
            return compte !== userID.toString();
          });
          // Si le Joueur n'a qu'un seul compte, fallback vide
          if (comptes.length === 0) {
            displayEmptyFallback();
          } else {
            // Si le Joueur a des multicomptes en dehors du userID actuel, loop l'array des multicomptes pour afficher les données
            Promise.all(comptes.map(fetchAccountData)).then(function (links) {
              links.filter(link => link !== null).forEach(link => {
                  profileLinksContainer.append(link);
              });
            });
          }
        } else {
          displayEmptyFallback();
        }
    });

    // Fonction pour récupérer les informations des multicomptes si il y en a
    function fetchAccountData(accountID) {
      return new Promise(function (resolve) {
        // Fetch chaque multicompte dans la table Registre de la base de données
        supabaseClient.from('Registre').select('*').eq('id', accountID).then(({ data, error }) => {
          if (error) {
              console.error('Error fetching data from Registre:', error.message);
              resolve(null);
              return;
          }
          // Affiche les données présentes pour chaque compte
          if (data && data.length > 0) {
            var accountData = data[0];
            var userUrl = '/u' + accountID;    

            var profileCard = $('<a>').attr('href', userUrl);

            // Affiche l'avatar du Registre
            var profileImage = $('<img>').attr('src', accountData.image).attr('alt', accountData.nom);

            // Logique en fonction du contenu des jobs et des études avec les champs nullable
            let job1 = accountData.occupation1;
            let job2 = accountData.occupation2;
            let jobdet1 = accountData.occupation_details1;
            let jobdet2 = accountData.occupation_details2;
            let cursus1 = accountData.cursus1;
            let cursus2 = accountData.cursus2;
            
            // Define userOccupText at the start to avoid redefining it in each block
            var userOccupText = "";

            if (job1 === "Étudiant.e" && job2 === "Étudiant.e") {
                userOccupText = "Étudiant.e en " + cursus1 + " et " + cursus2;
            } 
            else if (job1 === "Étudiant.e" && (job2 === null || job2 === "")) {
                userOccupText = "Étudiant.e en " + cursus1;
            } 
            else if (job1 === "Étudiant.e" && job2 !== "Étudiant.e") {
                userOccupText = "Étudiant.e en " + cursus1 + (jobdet2 ? ", " + jobdet2 : "");
            } 
            else if (job2 === "Étudiant.e" && job1 !== "Étudiant.e") {
                userOccupText = "Étudiant.e en " + cursus2 + (jobdet1 ? ", " + jobdet1 : "");
            } 
            else if (job1 !== "Étudiant.e" && (!job2 || job2 === "")) {
                userOccupText = jobdet1;
            } 
            else if (job2 !== "Étudiant.e" && (!job1 || job1 === "")) {
                userOccupText = jobdet2;
            } 
            else if (job1 !== "Étudiant.e" && job1 !== "" && job1 !== null && job2 !== "Étudiant.e" && job2 !== "" && job2 !== null) {
                userOccupText = jobdet1 + ", " + jobdet2;
            } 
            else if ((!job1 || job1 === "") && (!job2 || job2 === "")) {
                userOccupText = "Sans emploi";
            }


            var userName = $('<span>').addClass("nom").append(accountData.nom);
            var userOccup = $('<span>').addClass('occupation').append(userOccupText);
            var userBox = $('<div>').addClass('infos').append(userName).append(userOccup);

            profileCard.append(profileImage).append(userBox);

            // Ajoute la maison pour le color code de chaque card Multicompte
            if (accountData.maison === "Dojin") {
              usercolorClass = "gp-dojin";
            } else if (accountData.maison === "Hiisa") {
              usercolorClass = "gp-hiisa";
            } else if (accountData.maison === "Fugi") {
              usercolorClass = "gp-fugi";
            } else if (accountData.maison === "Raiyo") {
              usercolorClass = "gp-raiyo";
            } else if (accountData.maison === "Suisei") {
              usercolorClass = "gp-suisei";
            } else {
              usercolorClass = "no-group";
            }
            profileCard.addClass(usercolorClass);

            resolve(profileCard);
          } else {
            resolve(null);
          }
        });
      });
    }

    // Fonction Fallback si pas de multicomptes
    function displayEmptyFallback() {
      jQuery("#regular").find("div.multicomptes").addClass("empty");
    }
  };
});