var Wombat = function () {
    "use strict";

    function isGuest() {
        return -1 == parseInt(((my_getcookie("fa_" + location.host.replace(/\./g, "_") + "_data") || "").match(/"userid";(?:s:[0-9]+:"|i:)([0-9]+)/) || [0, -1])[1]);
    }

    const defaultOptions = {
        excludes: [],
        allowGuests: false,
        selector: "#wombat",
        displayOnLoad: "",
        overlay: true,
        overlayClass: "wombat-overlay",
        drawerClass: "wombat-aside",
        afterLoad: function () {}
    };

    function Wombat(options) {
        if (!new.target) throw "Wombat() doit être utilisé comme constructeur et déclaré avec le mot-clef new.";

        this.dependencies = {
            switcheroo: typeof Switcheroo !== "undefined"
        };
        this.options = Object.assign({}, defaultOptions, options);

        if (!this.options.allowGuests && isGuest()) {
            return;
        }

        this.transitionEnd = (function () {
            var testElement = document.createElement("div");
            return testElement.style.WebkitTransition ? "webkitTransitionEnd" : (testElement.style.OTransition ? "oTransitionEnd" : "transitionend");
        })();

        this.onClick();
    }

    Wombat.prototype.build = function () {
        var fragment = document.createDocumentFragment();
        this.aside = document.createElement("aside");
        this.aside.className = this.options.drawerClass;
        if (this.content) {
            this.aside.appendChild(this.content);
        }

        fragment.appendChild(this.aside);

        if (this.options.overlay) {
            this.overlay = document.createElement("div");
            this.overlay.className = this.options.overlayClass;
            fragment.appendChild(this.overlay);
        }

        document.body.appendChild(fragment);

        window.getComputedStyle(this.aside).height;
        this.aside.classList.add("open");
        this.overlay && this.overlay.classList.add("open");

        // Logiques et fonctions custom pour IMO
        this.updateColor();
        this.removeWhiteSpace();
        this.replaceIcons();
        this.tabClickHandler();
        this.addIcon();
        this.colorDia();
        this.populateUserInfo();
        this.addMulticomptes();
        this.populateFaceclaimField();
        this.cloneContent('#field_id-6 div.field_uneditable', 'div.profil-msg > dd');
        this.cloneContent('#field_id-13 div.field_uneditable', 'div.profil-points > dd');
        this.cloneContent('#field_id12 div.field_uneditable', 'div.profil-dama > dd');
        this.cloneContent('#field_id10 div.field_uneditable', '#content_tab2 > span');
        this.cloneContent('#field_id27 div.field_uneditable', '.credits > span');

        if (_userdata.user_level !== 1) {
            jQuery('#wombat').find('.profil-liens').removeClass('admin');
        } else {
            jQuery('#wombat').find('.profil-liens').addClass('admin');
        }
        var adminLink = jQuery('#wombat').find('span#admin_link > a');
        var adminText = adminLink.text().trim();
        if (adminLink.length > 0) {
            adminLink.empty().text('Modifier le profil');
        } 

        if (typeof this.options.afterLoad === "function") {
            this.options.afterLoad(this.aside, this.overlay);
        }
    };

    Wombat.prototype.binds = function () {
        this.overlay && this.overlay.addEventListener("click", this.close.bind(this));
    };

    Wombat.prototype.onClick = function () {
        var excludes = this.options.excludes.join(",");
        document.querySelectorAll(excludes ? `a[href^="/u"]:not(${excludes})` : 'a[href^="/u"]').forEach(link => {
            link.addEventListener("click", event => {
                event.stopPropagation();
                event.preventDefault();
                var userId = new URL(link.href).pathname.replace(/\D/g, "");
                this.load(userId).then(() => this.open());
            });
        });
    };

    // IMO - Logique de traduction des syntaxes de couleurs
    Wombat.prototype.hexToRgb = function (hex) {
        if (!hex) {
            return '';
        }
        hex = hex.replace(/^#/, '');
        var bigint = parseInt(hex, 16);
        var r = (bigint >> 16) & 255;
        var g = (bigint >> 8) & 255;
        var b = bigint & 255;
        return r + ',' + g + ',' + b;
    };

    // IMO - Fonction pour MAJ la couleur du profil en fonction de la maison
    Wombat.prototype.updateColor = function () {
        var groupColor = jQuery('#wombat #username > span').css("color");
        if (groupColor) {
            var matches = groupColor.match(/rgb\(([^)]+)\)/);
            if (matches && matches[1]) {
                var rgbValues = matches[1].split(',').map(Number);
                jQuery('#wombat > div').attr('style', 
                    `--co2_rgb: ${rgbValues.join(',')} !important; --co2: rgb(${rgbValues.join(',')}) !important;`);
            } else {
                console.error('Color not found in the style attribute.');
            }
        }
    };

    // IMO - Fonction pour virer le nbsp random pour les membres dans les boutons de contact
    Wombat.prototype.removeWhiteSpace = function () {
        var profilContact = document.querySelector('.profil-contact');
        var childNodes = Array.from(profilContact.childNodes);
        childNodes.forEach(function(node) {
            if (node.nodeType === Node.TEXT_NODE) {
                var parentNode = node.parentNode;
                if (parentNode !== profilContact) {
                    return;
                }
                profilContact.removeChild(node);
            }
        });
    };

    // IMO - Fonction pour remplacer les icônes FA par des ionicons
    Wombat.prototype.replaceIcons = function () {
        var replacements = [
            { source: 'https://2img.net/images2.imgbox.com/fd/40/a84fgaVp_o.png', replacement: '<ion-icon name="mail"></ion-icon>' },
            { source: 'https://2img.net/images2.imgbox.com/94/db/dbgG5XaY_o.png', replacement: '<ion-icon name="person-circle"></ion-icon>' },
            { source: 'https://i.imgur.com/TqXDBUX.png', replacement: '<ion-icon name="document-text"></ion-icon>' },
            { source: 'https://i.imgur.com/dqeB3t2.png', replacement: '<ion-icon name="logo-instagram"></ion-icon>' },
            { source: 'https://i.imgur.com/q7sw2M1.png', replacement: '<ion-icon name="heart-circle"></ion-icon>' },
        ];
        var postProfileContact = jQuery('#wombat').find('.profil-contact a');
        postProfileContact.each(function () {
            var contactLink = $(this);
            replacements.forEach(function (replacement) {
                var targetImg = contactLink.find('img[src="' + replacement.source + '"]');
                if (targetImg.length) {
                    targetImg.replaceWith(replacement.replacement);
                }
            });
        });
    };
    
    // IMO - Fonction des onglets pour le contenu du profil
    Wombat.prototype.tabClickHandler = function () {
        var tabs = jQuery('#wombat').find('.tab');
        var contentDivs = jQuery('#wombat').find('.tab-content');
        var profilContainer = jQuery('#wombat').find('.profil-container');
        tabs.on('click', function() {
            var tabId = $(this).attr('id');
            tabs.removeClass('active');
            $(this).addClass('active');
            contentDivs.addClass('hidden');
            $('#content_' + tabId).removeClass('hidden');
            profilContainer.attr('data-active-tab', tabId);
        });
    };

    // IMO - Fonction pour ajouter l'icône personnalisée depuis le profil Forumactif
    Wombat.prototype.addIcon = function () {
        var iconType = jQuery('#wombat').find('#field_id18 div.field_uneditable').text().trim();
        var champIcon = jQuery('#wombat').find('#field_id9 div.field_uneditable').text().trim();
        var profilName = jQuery('#wombat').find('.profil-name-title');
        if (iconType === 'Ionicons') {
            var iconName = champIcon;
            var newIcon = $('<ion-icon></ion-icon>').attr('name', iconName);
            profilName.append(newIcon);
        } else if (iconType === 'Feather') {
            var iconName = champIcon;
            var newIcon = $('<i></i>').attr('data-feather', iconName);
            profilName.append(newIcon);
        } else if (iconType === 'Bootstrap') {
            var iconName = champIcon;
            var newIcon = $('<i></i>').attr('class', 'bi-' + iconName);
            profilName.append(newIcon);
        } else if (iconType === 'Iconify') {
            var iconName = champIcon;
            var newIcon = $('<iconify-icon></iconify-icon>').attr('icon', iconName);
            profilName.append(newIcon);
        }
    }

    // IMO - Fonction pour remplir et styler la div Couleur de Dialogue en fonction du profil Forumactif
    Wombat.prototype.colorDia = function () {
        var colorContainer = jQuery('#wombat').find('span.couleur-dia');
        var couleurSpan = jQuery('#wombat').find('#field_id7 div.field_uneditable');
        var couleurWrite = couleurSpan.text().trim();
        var clonedContent = couleurSpan.clone();
        colorContainer.append(clonedContent);
        if (!couleurWrite || couleurWrite === '&nbsp;' || couleurWrite === '-') {
          colorContainer[0].style.setProperty('--co3', 'var(--co3)');
        } else {
          colorContainer[0].style.setProperty('--co3', couleurWrite);
        }
    }

    // IMO - Fonction pour ajouter le faceclaim (fetch de la base de données)
    Wombat.prototype.populateFaceclaimField = function () {
        // Détermine le userID du personnage
        var dataUserId = jQuery('#wombat').attr('data-user-id');
        var userId = dataUserId ? dataUserId.replace('u', '') : null;
        const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);

        // IMO - Fetch à la bdd pour récupérer le faceclaim
        supabaseClient
            .from('Registre')
            .select('faceclaim')
            .eq('id', userId)
            .then(({ data, error }) => {
                if (error) {
                    console.error('Error fetching data from Registre:', error.message);
                    return;
                }

                if (data && data.length > 0) {
                    var faceclaim = data[0].faceclaim;
                    jQuery('#wombat').find('.faceclaim > span').text(faceclaim);
                } else {
                    // Fallback : si le personnage n'est pas recensé, récupère le faceclaim du profil FA
                    this.cloneContent('#field_id-11 div.field_uneditable', '.faceclaim > span');
                }
            });
    };

    // IMO - Fonction pour ajouter les infos du joueur (fetch à la base de données)
    Wombat.prototype.populateUserInfo = function () {
        // Détermine le userID du personnage
        var dataUserId = jQuery('#wombat').attr('data-user-id');
        var userID = dataUserId ? dataUserId.replace('u', '') : null;

        // IMO - Définit les div de destination des informations
        var statutContainer = jQuery('#wombat').find('.irl-contact > .statut');
        var mpContainer = jQuery('#wombat').find(".mp");
        var discordContainer = jQuery('#wombat').find(".discord");

        const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);

        // IMO - Fetch à la bdd pour récupérer les infos du joueur
        supabaseClient
            .from('Joueurs')
            .select('*')
            .contains('comptes', `"${userID}"`)
            .then(({ data, error }) => {
                if (error) {
                    console.error('Error fetching data from Joueurs:', error.message);
                    alert('Failed to fetch data from Joueurs.');
                    return;
                }

                if (data && data.length > 0) {
                    var pseudo = data[0].pseudo;
                    var pronoms = data[0].pronoms;
                    var statutPres = data[0].statutPres;
                    var mpOK = data[0].mpOK;
                    var discordOK = data[0].discordOK;
                    console.log(mpOK);

                    document.querySelector('#pseudo').textContent = pseudo;
                    document.querySelector('#pronoms').textContent = pronoms;

                    if (statutPres === 'Présent.e') {
                        statutContainer.addClass('pres');
                        statutContainer.append("Présent.e");
                    } else if (statutPres === 'Présence réduite') {
                        statutContainer.addClass('presred');
                        statutContainer.append("Présence réduite");
                    } else if (statutPres === 'Absent.e') {
                        statutContainer.addClass('abs');
                        statutContainer.append("Absent.e");
                    }

                    if (mpOK === false) {
                        mpContainer.addClass('closed');
                        var closedIcon = $('<ion-icon name="close-circle-outline"></ion-icon>');
                        mpContainer.append(closedIcon);
                    } else {
                        mpContainer.addClass('open');
                        var openIcon = $('<ion-icon name="checkmark-circle-outline"></ion-icon>');
                        mpContainer.append(openIcon);
                    }
                    if (discordOK === false) {
                        discordContainer.addClass('closed');
                        var closedIcon = $('<ion-icon name="close-circle-outline"></ion-icon>');
                        discordContainer.append(closedIcon);
                    } else {
                        discordContainer.addClass('open');
                        var openIcon = $('<ion-icon name="checkmark-circle-outline"></ion-icon>');
                        discordContainer.append(openIcon);
                    }

                } else {
                    // IMO - Fallback si le joueur n'est pas enregistré
                    console.log('User not found in Joueurs table.');

                    this.cloneContent('#field_id-8 div.field_uneditable', '#pseudo');
                    this.cloneContent('#field_id4 div.field_uneditable', '#pronoms');

                    statutContainer.addClass('pres');
                    statutContainer.append("Présent.e");
                    mpContainer.addClass('open');
                    var openIcon = $('<ion-icon name="checkmark-circle-outline"></ion-icon>');
                    mpContainer.append(openIcon);
                    discordContainer.addClass('open');
                    var openIcon = $('<ion-icon name="checkmark-circle-outline"></ion-icon>');
                    discordContainer.append(openIcon);
                }
            });
    };

    // IMO - Fonction pour ajouter les multicomptes du joueur (fetch à la base de données)
    Wombat.prototype.addMulticomptes = function () {
        // Définit la div de destination des infos
        var profileLinksContainer = jQuery('#wombat').find('div.multicomptes > div.multicomptes-list > span');
        const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);

        // Récupère le userID du personnage
        var dataUserId = jQuery('#wombat').attr('data-user-id');
        var userID = dataUserId ? dataUserId.replace('u', '') : null;

        // IMO - Fetch à la bdd pour récupérer les infos du joueur
        supabaseClient
            .from('Joueurs')
            .select('*')
            .contains('comptes', `"${userID}"`)
            .then(({ data, error }) => {
                if (error) {
                    console.error('Error fetching data from Joueurs:', error.message);
                    displayEmptyFallback();
                    return;
                }

                if (data && data.length > 0) {
                    let comptes = data[0].comptes;
                    // Exclut le userID actuel de la liste des multicomptes
                    comptes = comptes.filter(function (compte) {
                        return compte !== userID.toString();
                    });
                    // Si le joueur n'a qu'un compte, alors activer le fallback
                    if (comptes.length === 0) {
                        displayEmptyFallback();
                    } else {
                        // Si le joueur a des multicomptes, loop les données de l'array Multicomptes pour afficher les infos
                        Promise.all(comptes.map(fetchAccountData))
                            .then(function (links) {
                                links.filter(link => link !== null).forEach(link => {
                                    profileLinksContainer.append(link);
                                });
                            });
                    }
                } else {
                    displayEmptyFallback();
                }
            });

        // IMO - Fonction pour récupérer les infos des multicomptes si il y en a
        function fetchAccountData(accountID) {
            return new Promise(function (resolve) {
                // Fetch les infos de chaque multicompte ID (accountID) dans la table Registre
                supabaseClient
                    .from('Registre')
                    .select('image, maison, nom')
                    .eq('id', accountID)
                    .then(({ data, error }) => {
                        if (error) {
                            console.error('Error fetching data from Registre:', error.message);
                            resolve(null);
                            return;
                        }
                        // Affiche l'avatar du Registre et le lien vers le profil du multicompte
                        if (data && data.length > 0) {
                            var accountData = data[0];
                            var userUrl = '/u' + accountID;
                            var profileLink = $('<a>').attr('href', userUrl);
                            var profileImage = $('<img>')
                                .attr('data-src', accountData.image)
                                .addClass('lazyload')
                                .attr('alt', accountData.nom);
                            profileLink.append(profileImage);
                            resolve(profileLink);
                        } else {
                            resolve(null);
                        }
                    });
            });
        }

        // IMO - Fonction Fallback si pas de multicomptes
        function displayEmptyFallback() {
            jQuery('#wombat').find('div.multicomptes').addClass('empty');
            jQuery('#wombat').find('div.irl-bottom').addClass('nodc');
        }
    };

    // IMO - Fonction Cloner le contenu pour gérer les champs de profil FA de manière plus claire
    Wombat.prototype.cloneContent = function (sourceSelector, targetSelector) {
        var sourceContainer = jQuery('#wombat');
        var sourceField = sourceContainer.find(sourceSelector);
        var targetContainer = sourceContainer.find(targetSelector);
        var clonedContent = sourceField.clone();
        targetContainer.append(clonedContent);
    };

    Wombat.prototype.load = function (userId) {
        var url = "/u" + userId;
        var selector = this.options.selector;

        return fetch(url)
            .then(response => response.text())
            .then(html => {
                var profileContent = (new DOMParser()).parseFromString(html, "text/html").querySelector(selector);

                if (profileContent === null) {
                    throw new Error("Profile content not found");
                }

                var fragment = document.createDocumentFragment();
                fragment.appendChild(profileContent);
                this.content = fragment;

                return this.content;
            })
            .catch(error => {
                console.error(error);
            });
    };

    Wombat.prototype.close = function () {
        this.aside.classList.remove("open");
        this.overlay && this.overlay.classList.remove("open");
        this.clear(this.aside, this.overlay);
    };

    Wombat.prototype.onClickOutside = function () {
        var self = this;
        document.addEventListener("click", function (event) {
            if (!self.aside.contains(event.target)) {
                self.close();
            }
        });
    };

    Wombat.prototype.clear = function () {
        var elements = Array.from(arguments);
    
        elements.forEach(element => {
            element.addEventListener(this.transitionEnd, function () {
                if (element.parentNode) {
                    element.parentNode.removeChild(element);
                }
                element.removeEventListener(this.transitionEnd, arguments.callee);
            });
        });
    };

    Wombat.prototype.open = function () {
        this.build();
        this.binds();
        this.onClickOutside();
    };

    function convertTagsForLazyLoad() {
        document.querySelectorAll('img, iframe').forEach(el => {
            if (!el.classList.contains('lazyload')) {
                if (el.tagName.toLowerCase() === 'img' || el.tagName.toLowerCase() === 'iframe') {
                    el.dataset.src = el.src;
                    el.removeAttribute('src');
                }
                el.classList.add('lazyload');
            }
        });
    }
    convertTagsForLazyLoad();

    return Wombat;
}();
