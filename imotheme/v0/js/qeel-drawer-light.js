$(document).ready(function () {
    var drawerContainer = $('#qeel_drawer_container');
    var drawer = $('#qeel_drawer');
    var button = $('#qeel_button');

    // Liste des user IDs qui ont le statut de staff
    var staffUserIds = ['1', '3', '4', '96', '93', '226', '266', '81', '21', '92']; // Remplacez avec les IDs des utilisateurs

    // Initialiser le conteneur du drawer
    if (!drawer.length) {
        drawer = $('<div id="qeel_drawer"></div>');
        drawerContainer.append(drawer);
        drawer.css({
            display: 'grid',
            right: '-400px'
        });
    }

    // Event listener sur le bouton du QEEL
    button.on('click', toggleDrawer);

    // Fonction pour toggle le drawer
    function toggleDrawer() {
        var isDrawerVisible = drawer.css('right') === '0px';
        drawer.css('right', isDrawerVisible ? '-400px' : '0px');
        button.toggleClass('open', !isDrawerVisible);

        // Ne remplit le drawer que s'il est visible
        if (!isDrawerVisible) {
            populateDrawerContent();
        }
    }

    // Fonction pour remplir le drawer
    function populateDrawerContent() {
        // Base de la structure du contenu
        drawerContent = `
            <div>
                <h3>Actuellement connecté.es&nbsp;—&nbsp;<span id="online_number"></span></h3>
                <div id="qeel_online" class="loading"></div>
            </div>
            <div>
                <h3>Dernières 48h</h3>
                <div id="qeel_48h" class="loading"></div>
            </div>
        `;
        drawer.html(drawerContent);
        // Ajoute le nombre de personnes connectées dans le titre
        $('#online_number').load('/ #user_number > strong:first-of-type');

        // Fonctions pour charger le contenu de chaque section
        loadOnlineContent();
        loadRecentContent();
    }

    // Charger la liste des membres en ligne
    function loadOnlineContent() {
        $('#qeel_online').load('/ div#loggedin', function () {

            // Retire le texte par défaut sur FA
            var pifContents = $('#qeel_online div#loggedin').contents();
            pifContents.each(function () {
                if (this.nodeType === 3) {
                    this.nodeValue = this.nodeValue.replace(/Utilisateurs enregistrés : Aucun/g, 'Aucun membre connecté');
                    this.nodeValue = this.nodeValue.replace(/Utilisateurs enregistrés : /g, '');
                    this.nodeValue = this.nodeValue.replace(/, /g, '');
                }
            });

            // Ajouter le statut de staff aux liens d'utilisateurs appropriés
            var userLinksOnline = $('#qeel_online div#loggedin a');
            userLinksOnline.each(function () {
                var userLink = $(this);
                var userId = getUserIdFromHref(userLink.attr('href'));
                if (staffUserIds.includes(userId)) {
                    userLink.append('<span class="tag">staff</span>');
                }
            });

            // Timeout de chargement pour le skeleton
            setTimeout(function () {
                $('#qeel_online').removeClass('loading');
            }, 1000);
        });
    }

    // Charger la liste des membres connectés pendant les dernières 48h
    function loadRecentContent() {
        $('#qeel_48h').load('/ div#latest', function () {

            // Retire le texte par défaut sur FA
            var kaboumContents = $('#qeel_48h div#latest').contents();
            kaboumContents.each(function () {
                if (this.nodeType === 3) {
                    this.nodeValue = this.nodeValue.replace(/Membres connectés au cours des 48 dernières heures : /g, '');
                    this.nodeValue = this.nodeValue.replace(/, /g, '');
                }
            });

            // Ajouter le statut de staff aux liens d'utilisateurs appropriés
            var userLinks48h = $('#qeel_48h div#latest a');
            userLinks48h.each(function () {
                var userLink = $(this);
                var userId = getUserIdFromHref(userLink.attr('href'));
                if (staffUserIds.includes(userId)) {
                    userLink.append('<span class="tag">staff</span>');
                }
            });

            // Timeout de chargement pour le skeleton
            setTimeout(function () {
                $('#qeel_48h').removeClass('loading');

                // Loope la liste des membres connectés et retire les membres en ligne de la liste des 48h pour éviter les doublons
                var userLinksPif = $('#qeel_online div#loggedin a');
                userLinksPif.each(function () {
                    var usernamePif = $(this).text().trim();
                    $('#qeel_48h div#latest a:contains("' + usernamePif + '")').remove();
                });
                
                // Placeholder si la liste est vide
                if ($('#qeel_48h div#latest a').length === 0) {
                    $('#qeel_48h div#latest').text('Il semblerait que tout le monde soit en ligne !');
                }

            }, 1000);
        });
    }

    // Fonction pour récupérer chaque userID depuis l'URL du lien sur le nom
    function getUserIdFromHref(href) {
        var matches = href.match(/\/u(\d+)$/);
        return matches ? matches[1] : null;
    }
});
