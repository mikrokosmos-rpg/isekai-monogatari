// LOGIQUE DU FORMULAIRE DU REGISTRE ET DE L'AFFICHAGE CONDITIONNEL DES CHAMPS

// LOGIQUE POUR REMPLIR LES LIEUX DE TRAVAIL
// Définit les deux select à remplir
const lieu1Select = document.getElementById('occupation_lieu1');
const lieu2Select = document.getElementById('occupation_lieu2');
// Fetch le JSON qui liste les lieux de travail disponibles sur IMO
function fetchJSONData() {
    const jsonFileURL = 'https://mikrokosmos-rpg.gitlab.io/isekai-monogatari/imo-directory/imo_jobs.json';
    fetch(jsonFileURL)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            const uniqueNomValues = [...new Set(data.map(item => item.nom))];
            populateDropdown(lieu1Select, uniqueNomValues);
            populateDropdown(lieu2Select, uniqueNomValues);
        })
        .catch(error => {
            console.error('Error fetching JSON:', error);
        });
}
// Remplit les 2 select des lieux de travail avec les lieux dans le JSON
function populateDropdown(selectElement, optionsArray) {
    optionsArray.forEach(optionText => {
        const option = document.createElement('option');
        option.value = optionText;
        option.text = optionText;
        selectElement.appendChild(option);
    });
}
// Initialise le fetch
document.addEventListener('DOMContentLoaded', () => {
    fetchJSONData();
});


// LOGIQUE DU FORMULAIRE
// Définit chaque input/select/checkbox dans le formulaire
const typeSelect = document.getElementById('type');
const urlDiv = document.getElementById('plLien');
const parti1Select = document.getElementById('parti1');
const parti2Select = document.getElementById('parti2');
const roninDiv = document.getElementById('ronin_pouvoir');
const roninDetailsInput = document.getElementById('ronin_details');
const animalDiv = document.getElementById('animal_forme');
const animalInput = document.getElementById('animal');
const mangetsukiDiv = document.getElementById('mangetsuki_role');
const mangetsukiDetailsSelect = document.getElementById('mangetsuki_details');
const occup1Select = document.getElementById('occupation1');
const occup2Select = document.getElementById('occupation2');
const occup1Details = document.getElementById('occup1_details');
const occup1DetailsInput = document.getElementById('occupation_details1');
const occup2Details = document.getElementById('occup2_details');
const occup2DetailsInput = document.getElementById('occupation_details2');
const etudiantDiv = document.querySelector('.formsection.etudiant');
const cosmoSelect = document.getElementById('cosmoball');
const cosmoptionsDiv = document.getElementById('cosmo_options');

// Affiche le champ plURL si le personnage est de type pré-lien ou scénario
function toggleURLSelect() {
    const selectedType = typeSelect.value;
    const showURLSelect = selectedType === 'Pré-lien' || selectedType === 'Scénario';
    if (showURLSelect) {
        urlDiv.style.display = 'grid';
    } else {
        // Si on change le personnage à Inventé, vide le champ URL et le retire
        urlDiv.style.display = 'none';
        showURLSelect.value = '';
    }
}

// Logique du champ occupation1
function toggleOccup1Details() {
    const selectedOccup1 = occup1Select.value;
    const showOccup1Details = selectedOccup1 === '' || selectedOccup1 === 'Étudiant.e';
    if (showOccup1Details) {
        // Si occupation est Étudiant, alors on vide et cache le occupation_details
        occup1Details.style.display = 'none';
        occup1DetailsInput.value = '';
    } else {
        occup1Details.style.display = 'flex';
    }
}
// Logique du champ occupation2
function toggleOccup2Details() {
    const selectedOccup2 = occup2Select.value;
    const showOccup2Details = selectedOccup2 === '' || selectedOccup2 === 'Étudiant.e';
    if (showOccup2Details) {
        // Si occupation est Étudiant, alors on vide et cache le occupation_details
        occup2Details.style.display = 'none';
        occup2DetailsInput.value = '';
    } else {
        occup2Details.style.display = 'flex';
    }
}
// Logique du champ Ronin_details
function toggleRoninDetails() {
    const selectedParti1 = parti1Select.value;
    const selectedParti2 = parti2Select.value;
    const showRoninDetails = selectedParti1 === 'Rōnin' || selectedParti2 === 'Rōnin';
    if (showRoninDetails) {
        // Si une des particularités est Ronin, afficher le champ des détails du ronin
        roninDiv.style.display = 'inline-flex';
    } else {
        // Sinon, vider et planquer le champ
        roninDiv.style.display = 'none';
        roninDetailsInput.value = '';
    }
}
// Logique du champ Animal
function toggleAnimalInput() {
    const selectedParti1 = parti1Select.value;
    const selectedParti2 = parti2Select.value;
    const showAnimalInput = selectedParti1 === 'Henjū' || selectedParti2 === 'Henjū' ||
        selectedParti1 === 'Jūjin' || selectedParti2 === 'Jūjin';
    if (showAnimalInput) {
        // Si une des particularités est Henju ou Juujin, afficher le champ de la forme animale
        animalDiv.style.display = 'inline-flex';
    } else {
        // Sinon, vider et planquer le champ
        animalDiv.style.display = 'none';
        animalInput.value = '';
    }
}
// Logique du champ Mangetsuki_details
function toggleMangetsukiDetailsSelect() {
    const selectedParti1 = parti1Select.value;
    const selectedParti2 = parti2Select.value;
    const showMangetsukiDetailsSelect = selectedParti1 === 'Mangetsuki' || selectedParti2 === 'Mangetsuki';

    if (showMangetsukiDetailsSelect) {
        // Si une des particularités est Mangetsuki, afficher le select du rôle dans la meute
        mangetsukiDiv.style.display = 'inline-flex';
    } else {
        // Sinon, vider et planquer le champ
        mangetsukiDiv.style.display = 'none';
        mangetsukiDetailsSelect.value = '';
    }
}
// Logique des champs Étudiants
function toggleEtudiantDetails() {
    const selectedOccup1 = occup1Select.value;
    const selectedOccup2 = occup2Select.value;
    const etudiantInputs = document.querySelectorAll('.formsection.etudiant input, .formsection.etudiant select');
    const showEtudiantDetails = selectedOccup1 === 'Étudiant.e' || selectedOccup2 === 'Étudiant.e';
    if (showEtudiantDetails) {
        // Si l'une des occupations est Étudiant, afficher le formulaire des infos étudiantes
        etudiantDiv.style.display = 'grid';
    } else {
        // Sinon, vider et planquer tous les champs étudiants
        etudiantDiv.style.display = 'none';
        etudiantInputs.forEach(element => {
            if (element.tagName === 'INPUT' && element.type === 'text') {
                element.value = '';
            } else if (element.tagName === 'SELECT') {
                element.selectedIndex = 0;
            }
        });
    }
}
// Logique du champ Cosmoball
function toggleCosmoSelect() {
    const selectedCosmo = cosmoSelect.value;
    const cosmoInputs = document.querySelectorAll('#cosmo_options input, #cosmo_options select');
    const showCosmoSelect = selectedCosmo === 'TRUE';
    if (showCosmoSelect) {
        // Si le champ Cosmoball est "Oui", afficher le formulaire des infos cosmoball
        cosmoptionsDiv.style.display = 'grid';
    } else {
        // Sinon, vider et planquer tous les champs
        cosmoptionsDiv.style.display = 'none';
        cosmoInputs.forEach(element => {
            if (element.tagName === 'INPUT' && element.type === 'text') {
                element.value = '';
            } else if (element.tagName === 'SELECT') {
                element.selectedIndex = 0;
            }
        });
    }
}

// Tous les listeners sur les select qui affectent la visiblité d'autres champs lorsqu'ils sont modifiés
typeSelect.addEventListener('change', () => {
    toggleURLSelect();
});
parti1Select.addEventListener('change', () => {
    toggleRoninDetails();
    toggleAnimalInput();
    toggleMangetsukiDetailsSelect();
});
parti2Select.addEventListener('change', () => {
    toggleRoninDetails();
    toggleAnimalInput();
    toggleMangetsukiDetailsSelect();
});
occup1Select.addEventListener('change', () => {
    toggleOccup1Details();
    toggleEtudiantDetails();
});
occup2Select.addEventListener('change', () => {
    toggleOccup2Details();
    toggleEtudiantDetails();
});
cosmoSelect.addEventListener('change', () => {
    toggleCosmoSelect();
});
toggleURLSelect();
toggleRoninDetails();
toggleAnimalInput();
toggleMangetsukiDetailsSelect();
toggleOccup1Details();
toggleOccup2Details();
toggleEtudiantDetails();
toggleCosmoSelect();

// Logique du champ École
const ecoleSelect = document.getElementById('ecole');
const otherEcoleInput = document.getElementById('otherEcoleInput');
const occupationInputs = document.querySelectorAll('input[name="occupation[]"]');
ecoleSelect.addEventListener('change', () => {
    if (ecoleSelect.value === 'Autre') {
        // Si l'école est "Autre", afficher le champ OtherEcole
        otherEcoleInput.style.display = 'flex';
    } else {
        // Sinon, vider et planquer le champ
        otherEcoleInput.style.display = 'none';
        document.getElementById('otherEcole').value = '';
    }
});
