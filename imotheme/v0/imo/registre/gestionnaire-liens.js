// GESTIONNAIRE DE LIENS

const supabaseUrl = 'https://gbejdguvqhsdvwtmqfbp.supabase.co';
const supabaseAnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdiZWpkZ3V2cWhzZHZ3dG1xZmJwIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA0NjUxOTksImV4cCI6MjAyNjA0MTE5OX0.kK8RMRPvXehp86KIM-sXfq9qERgaSLEFOqXuwZVZUqw';
const { createClient } = supabase;

$(document).ready(function() {
    const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);

    // Récupérer le userID du membre
    const userID = _userdata.user_id;
    // const userID = 3; // User test (Himari)

    // Cleaner les paramètres de query de l'URL au refresh
    function removeQueryParameters() {
        if (window.location.search) {
            const newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;
            window.history.replaceState({}, document.title, newUrl);
        }
    }

    // Fetch des infos dans la base de données
    supabaseClient
        .from('Registre')
        .select('*')
        .eq('id', userID)
        .then(({ data: userData, error: userError }) => {
            if (userError) {
                console.error('Error fetching user data:', userError.message);
                return;
            }

            const exists = userData.length > 0;
            if (!exists) {
                // Si le personnage n'est pas dans le Registre
                console.log("Pas d'entrée pour cet userID dans le registre :", userID);
                // Affiche le bon message d'alerte
                $('#message-new').show();
                $('#message-edit').hide();
                // Planque le formulaire de gestion des liens
  		        $('#editLiens').hide();
  		        $('#liensForm').hide();
            } else {
                // Si le personnage existe dans le Registre
  		        const data = userData[0];
                // Affiche le bon message d'intro
                $('#message-new').hide();
                $('#message-edit').show();
                // Affiche le formulaire de gestion des liens
  		        $('#editLiens').show();
  		        $('#liensForm').show();
  
                // S'assure que la div de destination pour ajouter les liens est vide
   		        $('.gesli').empty();

                // Si la colonne Liens n'est pas vide dans la base de données
                if (data.liens !== null) {
                    // Loop dans l'array pour afficher chaque lien
                    data.liens.liens.forEach(lien => {
                    // Remplace les line breaks HTML en retours de ligne WYSIWYG pour le textarea
  			        const descWithLineBreaks = lien["desc-long"].replace(/<br \/>/g, '\n');
                    // Structure HTML de chaque lien éditable
                    const lienHtml = `
                            <div class="lien-item" data-lien-id="${lien["lien-id"]}" style="order: ${lien["order"]};">
                                <img src="${lien["lien-img"]}" data-src="${lien["lien-img"]}" />
                                <input type="hidden" class="lien-id" name="lien-id" value="${lien["lien-id"]}" />
                                <input type="text" class="lien-nom" name="lien-nom" value="${lien["lien-nom"]}" />
                                <div class="type-order">
                                    <div class="field">
                                        <label for="lien-type">Type de lien</label>
                                        <select class="lien-type" name="lien-type">
                                            <option value="Positif" ${lien["lien-type"] === "Positif" ? 'selected' : ''}>Positif</option>
                                            <option value="Neutre" ${lien["lien-type"] === "Neutre" ? 'selected' : ''}>Neutre</option>
                                            <option value="Négatif" ${lien["lien-type"] === "Négatif" ? 'selected' : ''}>Négatif</option>
                                            <option value="Autre" ${lien["lien-type"] === "Autre" ? 'selected' : ''}>Autre</option>
                                        </select>
                                    </div>
                                    <div class="field">
                                        <label for="order">Ordre</label>
                                        <input type="text" class="order" name="order" ${lien["order"] !== "999" ? `value="${lien["order"]}"` : ''} />
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="desc-short">Description courte</label>
                                    <input type="text" class="desc-short" name="desc-short" value="${lien["desc-short"]}" />
                                </div>
                                <div class="field">
                                    <label for="desc-long">Description longue</label>
                                    <textarea class="desc-long" name="desc-long">${descWithLineBreaks}</textarea>
                                    <span class="hint">Tu peux utiliser les balises habituelles de HTML.</span>
                                </div>
                                <div class="field">
                                    <label for="lien-img">URL d'une image</label>
                                    <input type="text" class="lien-img" name="lien-img" value="${lien["lien-img"]}" />
                                </div>
                                <label for="delete-checkbox"><input type="checkbox" class="delete-checkbox" name="delete-checkbox" /> Supprimer ce lien</label>
                            </div>`;
                    // Affiche chaque lien dans la bonne div de destination selon le type de lien
                    if (lien["lien-type"] === "Positif") {
                        $('#gesli-pos').append(lienHtml);
                    } else if (lien["lien-type"] === "Neutre") {
                        $('#gesli-neu').append(lienHtml);
                    } else if (lien["lien-type"] === "Négatif") {
                        $('#gesli-neg').append(lienHtml);
                    } else if (lien["lien-type"] === "Autre") {
                        $('#gesli-misc').append(lienHtml);
                    }
                });

                // Affiche des messages si une catégorie de lien est vide
                if ($('#gesli-pos').is(':empty')) {
                    $('#gesli-pos').html('<div>Aucun lien positif trouvé.</div>');
			        $('button.gesli-pos').hide();
                }
                if ($('#gesli-neu').is(':empty')) {
                    $('#gesli-neu').html('<div>Aucun lien neutre trouvé.</div>');
			        $('button.gesli-neu').hide();
                }
                if ($('#gesli-neg').is(':empty')) {
                    $('#gesli-neg').html('<div>Aucun lien négatif trouvé.</div>');
			        $('button.gesli-neg').hide();
                }
                if ($('#gesli-misc').is(':empty')) {
                    $('#gesli-misc').html('<div>Aucun autre lien trouvé.</div>');
			        $('button.gesli-misc').hide();
                }
            } else {
                // Affiche les messages et planque les boutons Update si aucune donnée n'est trouvée dans la colonne Liens
                $('#gesli-pos').html('<div>Aucun lien positif trouvé.</div>');
		        $('button.gesli-pos').hide();
                $('#gesli-neu').html('<div>Aucun lien neutre trouvé.</div>');
		        $('button.gesli-neu').hide();
                $('#gesli-neg').html('<div>Aucun lien négatif trouvé.</div>');
		        $('button.gesli-neg').hide();
                $('#gesli-misc').html('<div>Aucun autre lien trouvé.</div>');
	            $('button.gesli-misc').hide();
            }
        }
    })
    .catch(error => {
        console.error('Error:', error.message);
        alert('Failed to fetch user data.');
    });

    // Logique d'update des liens
    $('#editLiens').submit(async function(e) {
        e.preventDefault();
        // Prépare le array à envoyer
        const updatedLiens = { "liens": [] };
        // Loope chaque lien affiché existant dans la page
        $('.lien-item').each(function() {
            const lienItem = $(this);
            // Skip le lien si la checkbox Supprimer est checkée
            const deleteCheckbox = lienItem.find('.delete-checkbox');
            if (deleteCheckbox.prop('checked')) {
                return;
            }
            // Mappe les données dans les fields du lien
            const lienId = lienItem.find('.lien-id').val();
            const nom = lienItem.find('.lien-nom').val();
            const type = lienItem.find('.lien-type').val();
            const descShort = lienItem.find('.desc-short').val();
            const descLong = lienItem.find('.desc-long').val();
	        const img = lienItem.find('.lien-img').val();
            let order = lienItem.find('.order').val();
            // Assure d'afficher un ordre élevé si vide pour que le lien s'affiche en dernier
            if (!order.trim()) {
                order = "999";
            }
            // Traduit les potentiels restes de BBCode en HTML
	        let descWithBreaks = descLong.replace(/\n/g, '<br />')
				    .replace(/\[b\]/g, '<strong>')
                    .replace(/\[\/b\]/g, '</strong>')
				    .replace(/\[i\]/g, '<i>')
                    .replace(/\[\/i\]/g, '</i>')
				    .replace(/\[u\]/g, '<u>')
                    .replace(/\[\/u\]/g, '</u>')
				    .replace(/\[url="([^"]*)"]([^[]*?)\[\/url\]/g, '<a href="$1">$2</a>');
            // Formatte le contenu de chaque lien en JSON pour l'array
            const newLien = {
                "lien-id": lienId,
                "lien-nom": nom,
                "lien-type": type,
                "desc-short": descShort,
                "desc-long": descWithBreaks,
		        "lien-img": img,
                "order": order,
            };
            // Ajoute chaque lien au array JSON final à envoyer
            updatedLiens.liens.push(newLien);
        });
    
        // Lance la fonction d'envoi des données
        try {
            // Update la colonne liens avec le JSON final mis à jour
            const { data, error } = await supabaseClient
                .from('Registre')
                .update({ liens: updatedLiens })
                .eq('id', userID);
            if (error) {
                console.error('Error updating liens:', error.message);
                return;
            }
	        alert('Tes modifications sont enregistrées !');
            location.reload();
        } catch (error) {
            console.error('Error updating liens:', error.message);
        }
    });

    // Fonction de création d'un nouveau lien
    $('#liensForm').submit(async function(e) {
        e.preventDefault();
        try {
            // Récupère le userID du personnage
            const userID = _userdata.user_id;
            // const userID = 3; // User test (Hima)
            
            // Mappe les données entrées dans le formulaire
            const nom = $('#lien-nom').val();
            const type = $('#lien-type').val();
            const descShort = $('#desc-short').val();
            const descLong = $('#desc-long').val();
	        const imgContent = $('#lien-img').val();
            // Définit le placeholder image par défaut si aucune image n'est ajoutée
	        const defaultImgPlaceholder = 'https://placehold.co/600x300';
	        const img = imgContent.trim() !== '' ? imgContent : defaultImgPlaceholder;
            // Assure d'afficher un ordre élevé si vide pour que le lien s'affiche en dernier
            let order = $('#ordre').val();
            if (!order.trim()) {
                order = "999";
            }
            // Traduit les restes de BBcode en HTML
	        let descWithBreaks = descLong.replace(/\n/g, '<br />')
                .replace(/\[b\]/g, '<strong>')
                .replace(/\[\/b\]/g, '</strong>')
                .replace(/\[i\]/g, '<i>')
                .replace(/\[\/i\]/g, '</i>')
                .replace(/\[u\]/g, '<u>')
                .replace(/\[\/u\]/g, '</u>')
                .replace(/\[url="([^"]*)"]([^[]*?)\[\/url\]/g, '<a href="$1">$2</a>');
    
            // Fetch le contenu existant de la colonne Liens pour cet userID
            const { data: userData, error: userError } = await supabaseClient
                .from('Registre')
                .select('liens')
                .eq('id', userID);
            if (userError) {
                console.error('Error fetching user data:', userError.message);
                return;
            }
            // Récupère l'array JSON existant
            let existingLiens = userData[0]?.liens;
            // Si vide, créer un nouvel array
            if (!existingLiens) {
                existingLiens = { "liens": [] };
            }
            // Si un array est déjà présent
            // Mappe tous les liens existants et récupère les lien-id pour récupérer la dernière valeur en date
            const existingLienIds = existingLiens && existingLiens.liens ? existingLiens.liens.map(lien => {
		    const id = parseInt(lien["lien-id"].split('-')[1]);
                return isNaN(id) ? 0 : id;
            }) : [];
            // Incrémente le futur nouvel ID pour le nouveau lien
            const maxLienId = existingLienIds.length > 0 ? Math.max(...existingLienIds) : 0;
            const newLienId = maxLienId + 1;
            // Formatte le nouvel ID pour le nouveau lien
            const lienId = `${userID}-${newLienId}`;
            // Formatte l'item du nouveau lien à ajouter à l'array existant
            const newLien = {
                "lien-id": lienId,
                "lien-nom": nom,
                "lien-type": type,
                "desc-short": descShort,
                "desc-long": descWithBreaks,
		        "lien-img": img,
                "order": order
            };
            // Ajoute le nouveau lien à l'array existant récupéré
            existingLiens.liens.push(newLien);
    
            // Update la colonne Liens de la base de données avec l'array JSON mis à jour
            const { error: updateError } = await supabaseClient
                .from('Registre')
                .update({ liens: existingLiens })
                .eq('id', userID);
            if (updateError) throw updateError;
            alert('Le nouveau lien a bien été ajouté !');
            location.reload();
        } catch (error) {
            console.error('Error:', error.message);
            alert('Oups, une erreur est survenue. Vérifie tes informations et réessaie, puis contacte le staff si le problème persiste !');
        }
    });
});