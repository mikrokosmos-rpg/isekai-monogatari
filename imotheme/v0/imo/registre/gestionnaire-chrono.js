// GESTIONNAIRE DE CHRONO

const supabaseUrl = 'https://gbejdguvqhsdvwtmqfbp.supabase.co';
const supabaseAnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdiZWpkZ3V2cWhzZHZ3dG1xZmJwIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA0NjUxOTksImV4cCI6MjAyNjA0MTE5OX0.kK8RMRPvXehp86KIM-sXfq9qERgaSLEFOqXuwZVZUqw';
const { createClient } = supabase;

$(document).ready(function() {
    const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);

    // Récupère le userID du membre
    const userID = _userdata.user_id;
    // const userID = 3; // User test (Hima)

    // Fonction pour cleaner les paramètres de query de l'URL au refresh
    function removeQueryParameters() {
        if (window.location.search) {
            const newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;
            window.history.replaceState({}, document.title, newUrl);
        }
    }

    // Fetch les données dans le Registre sur la database
    supabaseClient.from('Registre').select('*').eq('id', userID).then(({ data: userData, error: userError }) => {
        if (userError) {
            console.error('Error fetching user data:', userError.message);
            return;
        }
        const exists = userData.length > 0;
        if (!exists) {
            // Si le personnage n'existe pas dans le Registre
            console.log("Pas d'entrée pour cet userID dans le registre :", userID);
            // Afficher le message d'alerte
            $('#message-new').show();
            $('#message-edit').hide();
            // Planque le formulaire
            $('#editChrono').hide();
            $('#chronoForm').hide();
        } else {
            // Si le personnage existe dans le Registre
            const data = userData[0];
            // Afficher le message d'intro
            $('#message-new').hide();
            $('#message-edit').show();
            // Affiche le formulaire
            $('#editChrono').show();
            $('#chronoForm').show();
            $('.geschro').empty();  

            // Si la colonne Chrono contient des données
            if (data.chrono !== null) {      
                // Groupe chaque event par année   
                const groupedByYear = data.chrono.reduce((acc, event) => {
                    const year = event["annee"];
                    if (!acc[year]) {
                        acc[year] = [];
                    }
                    acc[year].push(event);
                    return acc;
                }, {});
                const sortedYears = Object.keys(groupedByYear).sort();

                // S'assure que la div de destination de la chrono est vide
                $('#geschro-timeline').empty();
            
                // Créer chaque groupe par année
                sortedYears.forEach(year => {
                    // Structure HTML de chaque groupe avec le titre (année)
                    const yearDiv = $(`<div class="year-group" data-year="${year}"><h4><span>${year}</span></h4></div>`);
                    // Pour chaque groupe, récupérer chaque event à afficher
                    groupedByYear[year].forEach(event => {
                        // Traduit les linebreaks HTML en sauts de ligne WYSIWYG pour le textarea
                        const descWithLineBreaks = event["desc"].replace(/<br \/>/g, '\n');
                        // Structure HTML de chaque event de chrono éditable
                        const eventHtml = `
                            <div class="chrono-item" data-lien-id="${event["chrono-id"]}" style="order: ${event["ordre"]};">
                                <input type="hidden" class="chrono-id" name="chrono-id" value="${event["chrono-id"]}" />
                                <div class="time">
                                    <input type="text" class="annee" name="annee" value="${event["annee"]}" />
                                    <input type="text" class="timestamp" name="timestamp" value="${event["timestamp"]}" />
                                </div>
                                <input type="text" class="ordre" name="ordre" value="${event["ordre"] !== "999" ? event["ordre"] : ''}" />
                                <div class="field">
                                    <label for="titre">Titre</label>
                                    <input type="text" class="titre" name="titre" value="${event["titre"]}" />
                                </div>
                                <div class="field">
                                    <label for="desc">Description</label>
                                    <textarea class="desc" name="desc">${descWithLineBreaks}</textarea>
                                    <span class="hint">Tu peux utiliser les balises habituelles de HTML.</span>
                                </div>
                                <label for="delete-checkbox">
                                    <input type="checkbox" class="delete-checkbox" name="delete-checkbox" /> Supprimer cet évènement
                                </label>
                            </div>`;
                        // Ajouter l'event à sa div année
                        yearDiv.append(eventHtml);
                    });
                    // Ajouter les divs années avec leurs titres à la div destination de la timeline
                    $('#geschro-timeline').append(yearDiv);
                });
                // Si la timeline est vide, afficher un message et planquer le bouton pour MAJ
                if ($('#geschro-timeline').is(':empty')) {
                    $('#geschro-timeline').html('<div>Aucun évènement.</div>');
                    $('button.geschro-timeline').hide();
                }
            } else {
                // Si le contenu de la colonne Chrono est vide, afficher un message et planquer le bouton pour MAJ
                $('#geschro-timeline').html('<div>Aucun évènement.</div>');
                $('button.geschro-timeline').hide();
            }
        }
    })
    .catch(error => {
        console.error('Error:', error.message);
        alert('Failed to fetch user data.');
    });

    $('#editChrono').submit(async function(e) {
        e.preventDefault();
    
        let updatedChrono = [];

        $('.chrono-item').each(function() {
            const chronoItem = $(this);
            const deleteCheckbox = chronoItem.find('.delete-checkbox');
            
            if (deleteCheckbox.prop('checked')) {
                return;
            }
        
            const chronoId = chronoItem.find('.chrono-id').val();
	        const annee = chronoItem.find('.annee').val();
            const timestamp = chronoItem.find('.timestamp').val();
            const titre = chronoItem.find('.titre').val();
            const desc = chronoItem.find('.desc').val();
            let ordre = chronoItem.find('.ordre').val();
            if (!ordre.trim()) {
                ordre = "999";
            }
 	        let descWithBreaks = desc.replace(/\n/g, '<br />')
				.replace(/\[b\]/g, '<strong>')
                .replace(/\[\/b\]/g, '</strong>')
				.replace(/\[i\]/g, '<i>')
                .replace(/\[\/i\]/g, '</i>')
				.replace(/\[u\]/g, '<u>')
                .replace(/\[\/u\]/g, '</u>')
				.replace(/\[url="([^"]*)"]([^[]*?)\[\/url\]/g, '<a href="$1">$2</a>');
        
            const newEvent = {
                "chrono-id": chronoId,
		        "annee": annee,
                "timestamp": timestamp,
                "titre": titre,
                "desc": descWithBreaks,
                "ordre": ordre,
            };
            updatedChrono.push(newEvent);
        });
        try {
            const { data, error } = await supabaseClient
                .from('Registre')
                .update({ chrono: updatedChrono })
                .eq('id', userID);
            if (error) {
                console.error('Error updating chrono:', error.message);
                return;
            }
	    alert('Tes modifications sont enregistrées !');
            location.reload();
        } catch (error) {
            console.error('Error updating chrono:', error.message);
        }
    });

    $('#chronoForm').submit(async function(e) {
        e.preventDefault();
        try {
	        const userID = _userdata.user_id;
	        // const userID = 3;
	        const annee = $('#annee').val();
            const timestamp = $('#timestamp').val();
            const titre = $('#titre').val();
            const desc = $('#desc').val();

            let ordre = $('#ordre').val();
            if (!ordre.trim()) {
                ordre = "999";
            }
 	        let descWithBreaks = desc.replace(/\n/g, '<br />')
				.replace(/\[b\]/g, '<strong>')
                .replace(/\[\/b\]/g, '</strong>')
				.replace(/\[i\]/g, '<i>')
                .replace(/\[\/i\]/g, '</i>')
				.replace(/\[u\]/g, '<u>')
                .replace(/\[\/u\]/g, '</u>')
				.replace(/\[url="([^"]*)"]([^[]*?)\[\/url\]/g, '<a href="$1">$2</a>');
    
            const { data: userData, error: userError } = await supabaseClient
                .from('Registre')
                .select('chrono')
                .eq('id', userID);
            if (userError) {
                console.error('Error fetching user data:', userError.message);
                return;
            }
    
            let existingChrono = userData[0]?.chrono;
            if (!existingChrono) {
                existingChrono = [];
            }
            const existingChronoIds = existingChrono.map(event => {
                const id = parseInt(event["chrono-id"].split('-')[1]);
                return isNaN(id) ? 0 : id;
            });
            const maxChronoId = existingChronoIds.length > 0 ? Math.max(...existingChronoIds) : 0;
            const newChronoId = maxChronoId + 1;
            const chronoId = `${userID}-${newChronoId}`;
            const newEvent = {
                "chrono-id": chronoId,
		        "annee": annee,
                "timestamp": timestamp,
                "titre": titre,
                "desc": descWithBreaks,
                "ordre": ordre
            };
            
            existingChrono.push(newEvent);
    
            const { error: updateError } = await supabaseClient
                .from('Registre')
                .update({ chrono: existingChrono })
                .eq('id', userID);
            if (updateError) throw updateError;
            alert('Le nouvel évènement a bien été ajouté !');
            location.reload();
        } catch (error) {
            console.error('Error:', error.message);
            alert('Oups, une erreur est survenue. Vérifie tes informations et réessaie, puis contacte le staff si le problème persiste !');
        }
    });
});