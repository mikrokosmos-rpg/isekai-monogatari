// GESTIONNAIRE DE MOODBOARD
const supabaseUrl = 'https://gbejdguvqhsdvwtmqfbp.supabase.co';
const supabaseAnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdiZWpkZ3V2cWhzZHZ3dG1xZmJwIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA0NjUxOTksImV4cCI6MjAyNjA0MTE5OX0.kK8RMRPvXehp86KIM-sXfq9qERgaSLEFOqXuwZVZUqw';
const { createClient } = supabase;

$(document).ready(function() {
    const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);

    // Récupère le userID du membre
    const userID = _userdata.user_id;
    // const userID = 3; // User test (Hima)
    const userName = _userdata.username;

    // Fonction pour cleaner les paramètres de query dans l'URL au refresh
    function removeQueryParameters() {
        if (window.location.search) {
            const newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;
            window.history.replaceState({}, document.title, newUrl);
        }
    }
    
    // Fonction pour générer le code snippet HTML
    function generateCode(imageUrl1, imageUrl2, imageUrl3, imageUrl4, imageUrl5, imageUrl6, imageUrl7, imageUrl8, imageUrl9) {
        const code = '<div class="imo-gesmoo">\n' +
                     '  <img src="' + imageUrl1 + '" data-src="' + imageUrl1 + '" alt="Image 1">\n' +
                     '  <img src="' + imageUrl2 + '" data-src="' + imageUrl2 + '" alt="Image 2">\n' +
                     '  <img src="' + imageUrl3 + '" data-src="' + imageUrl3 + '" alt="Image 3">\n' +
  		             '  <img src="' + imageUrl4 + '" data-src="' + imageUrl4 + '" alt="Image 4">\n' +
  		             '  <img src="' + imageUrl5 + '" data-src="' + imageUrl5 + '" alt="Image 5">\n' +
  		             '  <img src="' + imageUrl6 + '" data-src="' + imageUrl6 + '" alt="Image 6">\n' +
                     '  <img src="' + imageUrl7 + '" data-src="' + imageUrl7 + '" alt="Image 7">\n' +
                     '  <img src="' + imageUrl8 + '" data-src="' + imageUrl8 + '" alt="Image 8">\n' +
  		             '  <img src="' + imageUrl9 + '" data-src="' + imageUrl9 + '" alt="Image 9">\n' +
                     '</div>';
        $('#code-snippet code').text(code);
  	    hljs.highlightAll();
    }

    // Fetch l'entrée du Registre dans la database
    supabaseClient
    .from('Registre')
    .select('*')
    .eq('id', userID)
    .then(({ data: userData, error: userError }) => {
        if (userError) {
            console.error('Error fetching user data:', userError.message);
            return;
        }

        const exists = userData.length > 0;
        if (!exists) {
            // Si le personnage n'est pas au Registre
            console.log("Pas d'entrée pour cet userID dans le registre :", userID);
            // Afficher le message d'alerte
            $('#message-new').show();
            $('#message-edit').hide();
            // Planque le formulaire
            $('#editMood').hide();
        } else {
            // Si le personnage est dans le Registre
            const data = userData[0];
            // Afficher le message d'intro
            $('#message-new').hide();
            $('#message-edit').show();
            // Afficher le formulaire
            $('#editMood').show();

            const moodboard = data.moodboard;
            // Si la colonne moodboard contient des informations, mapper le contenu présent
            if (moodboard) {
                const imageUrl1 = moodboard.image1;
                const imageUrl2 = moodboard.image2;
                const imageUrl3 = moodboard.image3;
                const imageUrl4 = moodboard.image4;
                const imageUrl5 = moodboard.image5;
                const imageUrl6 = moodboard.image6;
                const imageUrl7 = moodboard.image7;
                const imageUrl8 = moodboard.image8;
                const imageUrl9 = moodboard.image9;
            
                // Ajoute les URL dans chaque form field
                $('#image-url-1').val(imageUrl1);
                $('#image-url-2').val(imageUrl2);
                $('#image-url-3').val(imageUrl3);
                $('#image-url-4').val(imageUrl4);
                $('#image-url-5').val(imageUrl5);
                $('#image-url-6').val(imageUrl6);
                $('#image-url-7').val(imageUrl7);
                $('#image-url-8').val(imageUrl8);
                $('#image-url-9').val(imageUrl9);

                // Affiche les images dans la preview du moodboard
                $('#image1 img').attr('src', imageUrl1);
                $('#image1 img').attr('data-src', imageUrl1);
                $('#image2 img').attr('src', imageUrl2);
                $('#image2 img').attr('data-src', imageUrl2);
                $('#image3 img').attr('src', imageUrl3);
                $('#image3 img').attr('data-src', imageUrl3);
                $('#image4 img').attr('src', imageUrl4);
                $('#image4 img').attr('data-src', imageUrl4);
                $('#image5 img').attr('src', imageUrl5);
                $('#image5 img').attr('data-src', imageUrl5);
                $('#image6 img').attr('src', imageUrl6);
                $('#image6 img').attr('data-src', imageUrl6);
                $('#image7 img').attr('src', imageUrl7);
                $('#image7 img').attr('data-src', imageUrl7);
                $('#image8 img').attr('src', imageUrl8);
                $('#image8 img').attr('data-src', imageUrl8);
                $('#image9 img').attr('src', imageUrl9);
                $('#image9 img').attr('data-src', imageUrl3);
            
                // Génère le snippet de code en HTML avec les URL de la database
                generateCode(imageUrl1, imageUrl2, imageUrl3, imageUrl4, imageUrl5, imageUrl6, imageUrl7, imageUrl8, imageUrl9);
            } else {
                // Si la colonne moodboard est vide, remplacer tous les inputs fields et la preview par une image placeholder
                const imageUrl = "https://placehold.co/600x600";

                $('#image1 img').attr('src', imageUrl);
                $('#image1 img').attr('data-src', imageUrl);
                $('#image2 img').attr('src', imageUrl);
                $('#image2 img').attr('data-src', imageUrl);
                $('#image3 img').attr('src', imageUrl);
                $('#image3 img').attr('data-src', imageUrl);
                $('#image4 img').attr('src', imageUrl);
                $('#image4 img').attr('data-src', imageUrl);
                $('#image5 img').attr('src', imageUrl);
                $('#image5 img').attr('data-src', imageUrl);
                $('#image6 img').attr('src', imageUrl);
                $('#image6 img').attr('data-src', imageUrl);
                $('#image7 img').attr('src', imageUrl);
                $('#image7 img').attr('data-src', imageUrl);
                $('#image8 img').attr('src', imageUrl);
                $('#image8 img').attr('data-src', imageUrl);
                $('#image9 img').attr('src', imageUrl);
                $('#image9 img').attr('data-src', imageUrl);
        
                // Planque le bloc de code HTML
                $('#formBox > h3').hide();
                $('#code-snippet').hide();
                console.log("Pas assez d'images dans le moodboard.");
            }
        } 
    })
    .catch(error => {
        console.error('Error:', error.message);
        alert('Failed to fetch user data.');
    });

    // Preview/MAJ automatique du moodboard en temps réel 
    $('.image-url').on('input', function() {
        // On récupère les URL dans les fields
        const imageUrl1 = $('#image-url-1').val();
        const imageUrl2 = $('#image-url-2').val();
        const imageUrl3 = $('#image-url-3').val();
        const imageUrl4 = $('#image-url-4').val();
        const imageUrl5 = $('#image-url-5').val();
        const imageUrl6 = $('#image-url-6').val();
        const imageUrl7 = $('#image-url-7').val();
        const imageUrl8 = $('#image-url-8').val();
        const imageUrl9 = $('#image-url-9').val();
        // On MAJ les images affichées
        $('#image1 img').attr('src', imageUrl1);
        $('#image1 img').attr('data-src', imageUrl1);
        $('#image2 img').attr('src', imageUrl2);
        $('#image2 img').attr('data-src', imageUrl2);
        $('#image3 img').attr('src', imageUrl3);
        $('#image3 img').attr('data-src', imageUrl3);
        $('#image4 img').attr('src', imageUrl4);
        $('#image4 img').attr('data-src', imageUrl4);
        $('#image5 img').attr('src', imageUrl5);
        $('#image5 img').attr('data-src', imageUrl5);
        $('#image6 img').attr('src', imageUrl6);
        $('#image6 img').attr('data-src', imageUrl6);
        $('#image7 img').attr('src', imageUrl7);
        $('#image7 img').attr('data-src', imageUrl7);
        $('#image8 img').attr('src', imageUrl8);
        $('#image8 img').attr('data-src', imageUrl8);
        $('#image9 img').attr('src', imageUrl9);
        $('#image9 img').attr('data-src', imageUrl9);
        // On MAJ le snippet HTML
        generateCode(imageUrl1, imageUrl2, imageUrl3, imageUrl4, imageUrl5, imageUrl6, imageUrl7, imageUrl8, imageUrl9);
    });

    // Fonction pour envoyer les données à la database
    $('#editMood').submit(async function(e) {
        // On affiche le spinner pendant que la fonction tourne
        $('#spinner').removeClass('hidden');
        $('#confirmButton').prop('disabled', true);

        e.preventDefault();

        // Mapping des données dans les fields
        const imageUrl1 = $('#image-url-1').val();
        const imageUrl2 = $('#image-url-2').val();
        const imageUrl3 = $('#image-url-3').val();
        const imageUrl4 = $('#image-url-4').val();
        const imageUrl5 = $('#image-url-5').val();
        const imageUrl6 = $('#image-url-6').val();
        const imageUrl7 = $('#image-url-7').val();
        const imageUrl8 = $('#image-url-8').val();
        const imageUrl9 = $('#image-url-9').val();
        // Formattage des données à envoyer en JSON
        const moodboardData =
            { 
                "image1": imageUrl1,
                "image2": imageUrl2,
                "image3": imageUrl3,
                "image4": imageUrl4,
                "image5": imageUrl5,
                "image6": imageUrl6,
                "image7": imageUrl7,
                "image8": imageUrl8,
                "image9": imageUrl9
            };

        try {
            // MAJ les données dans le Registre
            const { data, error } = await supabaseClient
            .from('Registre')
            .update({ moodboard: moodboardData })
            .eq('id', userID);
            if (error) {
                console.error('Error updating moodboard:', error.message);
                return;
            }
            alert('Tes modifications sont enregistrées !');
            location.reload();
        } catch (error) {
            console.error('Error updating moodboard:', error.message);
            alert('Oups, une erreur est survenue. Vérifie tes informations et réessaie, puis contacte le staff si le problème persiste !');
        } finally {
            // Une fois la fonction ayant terminé de process, on planque le spinner de nouveau et on réactive le bouton
            $('#spinner').addClass('hidden');
            $('#confirmButton').prop('disabled', false);
        }
    });
});

// Fonction Sélectionner et copier le code
const copyButton = document.getElementById('copyButton');
function copyToClipboard() {
    const codeElement = document.querySelector('#jsonOutput code');
    const textToCopy = codeElement.textContent;
    const textArea = document.createElement('textarea');
    textArea.value = textToCopy;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand('copy');
    document.body.removeChild(textArea);
    const copyButton = document.getElementById('copyButton');
    copyButton.innerHTML = '<ion-icon name="copy"></ion-icon> Code copié !';
    setTimeout(() => {
        copyButton.innerHTML = '<ion-icon name="copy"></ion-icon> Copier le code';
    }, 3000);
}