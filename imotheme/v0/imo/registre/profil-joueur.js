// GESTIONNAIRE DU PROFIL DE JOUEUR

const supabaseUrl = 'https://gbejdguvqhsdvwtmqfbp.supabase.co';
const supabaseAnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdiZWpkZ3V2cWhzZHZ3dG1xZmJwIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA0NjUxOTksImV4cCI6MjAyNjA0MTE5OX0.kK8RMRPvXehp86KIM-sXfq9qERgaSLEFOqXuwZVZUqw';
const { createClient } = supabase;

$(document).ready(function () {
    const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);

    // Récupère le userID du membre connecté
    const userID = _userdata.user_id;
    // const userID = 2; // User test (userID sans joueur)

    // Fonction pour clear les paramètres de query au reload de la page après envoi d'un formulaire
    function removeQueryParameters() {
        if (window.location.search) {
            const newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;
            window.history.replaceState({}, document.title, newUrl);
        }
    }

    // Check de base si il y a des données dispo pour ce userID
    supabaseClient.from('Joueurs')
        .select('*')
        .contains('comptes', `"${userID}"`)
        .then(({ data, error }) => {
            if (error) {
                console.error('Error fetching data from Joueurs:', error.message);
                alert('Failed to fetch data from Joueurs.');
                return;
            }

            // Process the data
            if (data && data.length > 0) {
                // SI LE JOUEUR EXISTE POUR CE USERID

                // Affichage des données pour ce cas de figure
                $('button#confirmButton span').text("Modifier mes informations");
                // Affichage du message d'intro
                $('#message-new').hide();
                $('#message-edit').show();
                // On planque le select multicompte et la select Pseudo puisque pas utiles
                $('#new-entry').hide();
                $('#multi').val("").removeAttr('required');
                $('select[name="pseudo-select"]').val("").removeAttr('required');

                // Ensuite, remplissage du form avec les données disponibles
                $('#pseudo').val(data[0].pseudo);
                $('#pronoms').val(data[0].pronoms);
                $('#statutPres').val(data[0].statutPres);
                if (data[0].mpOK === true) {
                    $('#mpOK').val("TRUE");
                } else {
                    $('#mpOK').val("FALSE");
                }
                if (data[0].discordOK === true) {
                    $('#discordOK').val("TRUE");
                } else {
                    $('#discordOK').val("FALSE");
                }
                $('#mainCompte').val(data[0].mainCompte);

                // Compteur de multicomptes
                const comptes = data[0].comptes.join(', ');
                const count = data[0].comptes.length;
                const comptesCountSpan = document.getElementById('comptes-count');
                comptesCountSpan.textContent = '(' + count + ')';

                // On formatte l'array JSON des multicomptes pour afficher la liste dans l'input
                $('#comptes').val(comptes);
                const comptesArray = data[0].comptes;
                // Mapping de l'array pour afficher chaque info de multicompte disponible
                const fetchPromises = comptesArray.map((id) => {
                    return supabaseClient.from('Registre')
                        .select('*')
                        .eq('id', id)
                        .then(({ data, error }) => {
                            if (error) {
                                console.error(`Error fetching data from Registre for ID ${id}:`, error.message);
                                return null;
                            }
                            if (data && data.length > 0) {
                                return { id, nom: data[0].nom, image: data[0].image, maison: data[0].maison };
                            } else {
                                // Placeholders si le personnage n'est pas au registre
                                return { id, nom: 'Personnage #' + id + ' non-recensé', image: 'https://placehold.co/400x400', maison: 'Maison' };
                            }
                        });
                });
                // Affichage des avatars et noms des multicomptes
                Promise.all(fetchPromises).then((results) => {
                    const validResults = results.filter((result) => result !== null);
                    renderMultiComptesList(validResults);
                }).catch((error) => {
                    console.error('Error fetching data from Registre:', error.message);
                    alert('Failed to fetch data from Registre.');
                });


            } else {
                // SI LE JOUEUR N'EXISTE PAS OU LE USERID N'EST PAS TROUVÉ

                console.log("Pas d'entrée pour cet userID dans le registre :", userID);
                // On ajuste ce qui est affiché dans le formulaire
                $('button#confirmButton span').text("Enregistrer mes informations");
                // Message d'alerte
                $('#message-new').show();
                $('#message-edit').hide();
                // Affichage du select "est-ce un multicompte"
                $('#new-entry').show();
                // Par défaut on planque le select Pseudo, qui reviendra si le compte est un multicompte
                $('div.pseudo-select').hide();
                $('select[name="pseudo-select"]').val("").removeAttr('required');

                // Auto-remplissage du select Pseudo avec les pseudos dans le Registre
                supabaseClient.from('Joueurs').select('pseudo').then(({ data, error }) => {
                    if (error) {
                        console.error('Error fetching data from Joueurs:', error.message);
                        alert('Failed to fetch data from Joueurs.');
                        return;
                    }
                    const selectElement = document.querySelector('select[name="pseudo-select"]');
                    if (!selectElement) {
                        console.error(`Select element not found.`);
                        return;
                    }
                    data.forEach((row) => {
                        if (!Array.from(selectElement.options).some(option => option.value === row.pseudo)) {
                            const option = document.createElement('option');
                            option.value = row.pseudo;
                            option.textContent = row.pseudo;
                            selectElement.appendChild(option);
                        }
                    });
                }).catch((error) => {
                    console.error('Error:', error.message);
                    alert('Failed to fetch data.');
                });

                // Fonction pour gérer la valeur du champ "Est-ce que ce compte est un multicompte"
                // NOTE: Peut être à améliorer avec un simple .hide() ?
                function handleMultiChange() {
                    // Set la valeur par défaut
                    const isMulti = document.querySelector('#multi').value === 'TRUE';
                    // Set le toggle de visibilité
                    const pseudoInputDisplay = isMulti ? 'none' : 'flex';
                    const pseudoSelectDisplay = isMulti ? 'flex' : 'none';
                    // Active le toggle sur les div en question
                    document.querySelectorAll('div.pseudo-input').forEach((element) => {
                        element.style.display = pseudoInputDisplay;
                    });
                    document.querySelectorAll('div.pseudo-select').forEach((element) => {
                        element.style.display = pseudoSelectDisplay;
                    });
                }
                // On surveille le changement de valeur du field et on initialise la fonction
                document.querySelector('#multi').addEventListener('change', handleMultiChange);
                handleMultiChange();

                // Fonction pour pré-remplir le formulaire après avoir sélectionné un pseudo dans la liste
                function handlePseudoSelectChange() {
                    // Récupère la valeur du select Pseudo
                    const selectedPseudo = $('select[name="pseudo-select"]').val();
                    // On récupère les données du pseudo dans la table Joueurs
                    supabaseClient.from('Joueurs')
                        .select('*')
                        .eq('pseudo', selectedPseudo)
                        .single()
                        .then(({ data, error }) => {
                            if (error) {
                                console.error('Error fetching data for selected pseudo:', error.message);
                                alert('Failed to fetch data for the selected pseudo.');
                                return;
                            }
                            // Si le pseudo a des données valides, remplissage du form
                            if (data) {
                                $('#pseudo').val(data.pseudo);
                                $('#pronoms').val(data.pronoms);
                                $('#statutPres').val(data.statutPres);
                                if (data.mpOK === true) {
                                    $('#mpOK').val("TRUE");
                                } else {
                                    $('#mpOK').val("FALSE");
                                }
                                if (data.discordOK === true) {
                                    $('#discordOK').val("TRUE");
                                } else {
                                    $('#discordOK').val("FALSE");
                                }
                                $('#mainCompte').val(data.mainCompte);
                                // Compteur de multicomptes
                                const comptes = data.comptes.join(', ');
                                const count = data.comptes.length;
                                const comptesCountSpan = document.getElementById('comptes-count');
                                comptesCountSpan.textContent = '(' + count + ')';
                                // Remplissage de la liste des multicomptes avec l'array JSON
                                $('#comptes').val(comptes);
                                const comptesArray = data.comptes;
                                // On map l'array pour afficher les données de chaque compte listé depuis la table Registre
                                const fetchPromises = comptesArray.map((id) => {
                                    return supabaseClient.from('Registre')
                                        .select('*')
                                        .eq('id', id)
                                        .then(({ data, error }) => {
                                            if (error) {
                                                console.error(`Error fetching data from Registre for ID ${id}:`, error.message);
                                                return null;
                                            }
                                            if (data && data.length > 0) {
                                                return { id, nom: data[0].nom, image: data[0].image, maison: data[0].maison };
                                            } else {
                                                // Placeholders si le personnage n'est pas au registre
                                                return { id, nom: 'Personnage #' + id + ' non-recensé', image: 'https://placehold.co/400x400', maison: 'Maison' };
                                            }
                                        });
                                });
                                Promise.all(fetchPromises).then((results) => {
                                    // On cleane les potentiels résultats null et on lance la fonction pour afficher les données
                                    const validResults = results.filter((result) => result !== null);
                                    renderMultiComptesList(validResults);
                                }).catch((error) => {
                                    console.error('Error fetching data from Registre:', error.message);
                                    alert('Failed to fetch data from Registre.');
                                });

                            }
                        })
                        .catch((error) => {
                            console.error('Error:', error.message);
                            alert('Failed to fetch data.');
                        });
                }
                // On surveille le changement de valeur du select Pseudo pour retrouver le bonnes données
                document.querySelector('.pseudo-select').addEventListener('change', handlePseudoSelectChange);
            }
        })
        .catch((error) => {
            console.error('Error:', error.message);
            alert('Failed to fetch data.');
        });

    // Fonction qui affiche les infos de chaque multicompte dans l'array
    function renderMultiComptesList(results) {
        // Définit la div de destination
        const multiComptesListDiv = document.getElementById('multicomptes-list');
        multiComptesListDiv.innerHTML = '';
        // Loope pour chaque accountID de multicompte
        results.forEach((result) => {
            console.log('Result object:', result);
            const listItem = document.createElement('a');
            listItem.setAttribute('href', `/u${result.id}`);
            listItem.setAttribute('class', `${result.maison}`);
            listItem.innerHTML = `
                <img data-src="${result.image}" src="${result.image}" alt="${result.nom}">
                <p>${result.nom}</p>
            `;
            multiComptesListDiv.appendChild(listItem);
        });
    }

    // Fonction d'envoi des données à la database
    $('#joueurForm').submit(async function (e) {
        e.preventDefault();

        // Quand la fonction est déclenchée, on affiche le spinner
        $('#spinner').removeClass('hidden');
        $('#confirmButton').prop('disabled', true);

        try {
            // On récupère l'ID du compte actuel
            const userID = _userdata.user_id;
            //const userID = 2; // User test
            
            // Mapping des infos du formulaire
            const pseudo = $('#pseudo').val();
            const pronoms = $('#pronoms').val();
            const statutPres = $('#statutPres').val();
            const mpOK = $('#mpOK').val();
            const discordOK = $('#discordOK').val();
            const mainCompte = $('#mainCompte').val();
            const comptesInput = $('#comptes').val();
            const comptesArray = comptesInput.split(',').map(str => str.trim());

            // Formatting des données à envoyer
            // En cas de nouveau joueur
            const formData1 = {
                id: userID,
                pseudo, pronoms, statutPres, mpOK, discordOK, mainCompte,
                comptes: comptesArray,
            };
            // En cas de mise à jour des infos existantes
            const formData2 = {
                pseudo, pronoms, statutPres, mpOK, discordOK, mainCompte,
                comptes: comptesArray,
            };

            console.log("formData1", formData1);
            console.log("formData2", formData2);

            // Check if le compte existe quelque part dans la base de données
            // const { data: compteData } = await supabaseClient.from('Joueurs').select('*').contains('comptes', `"${userID}"`);
            const { data: joueurData } = await supabaseClient.from('Joueurs').select('*').eq('pseudo', pseudo);

            console.log(joueurData);
            
            // const compteExists = compteData.length > 0;
            const joueurExists = joueurData.length > 0;
            if (!joueurExists) {
                // SI LE JOUEUR N'EXISTE PAS
                const { data, error: insertError } = await supabaseClient.from('Joueurs').insert([formData1]);
                if (insertError) throw insertError;
                location.reload();
                alert('Informations enregistrées !');
            } else {
                // SI LE JOUEUR EXISTE
                // Updates des données du pseudo correspondant
                const { data, error: updateError } = await supabaseClient.from('Joueurs').update([formData2]).eq('pseudo', formData2.pseudo).select('pseudo');
                if (updateError) throw updateError;
                // Refresh de page
                location.reload();
                removeQueryParameters();
                alert('Tes informations ont été mises à jour !');
            }
        } catch (error) {
            console.error('Error:', error.message);
            alert('Une erreur est survenue. Veuillez réessayer.');
        } finally {
            // Quand la fonction a fini de tourner, on planque le spinner
            $('#spinner').addClass('hidden');
            $('#confirmButton').prop('disabled', false);
        }
    });
});