const supabaseUrl = 'https://gbejdguvqhsdvwtmqfbp.supabase.co';
const supabaseAnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdiZWpkZ3V2cWhzZHZ3dG1xZmJwIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA0NjUxOTksImV4cCI6MjAyNjA0MTE5OX0.kK8RMRPvXehp86KIM-sXfq9qERgaSLEFOqXuwZVZUqw';
const { createClient } = supabase;

$(document).ready(function () {
    const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);

    const userID = _userdata.user_id;
    // const userID = 3; // User test (Hima)
    const userName = _userdata.username;

    function removeQueryParameters() {
        if (window.location.search) {
            const newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;
            window.history.replaceState({}, document.title, newUrl);
        }
    }

    // CHECK SI LE JOUEUR EST DANS LE REGISTRE
    supabaseClient.from('Joueurs').select('*').contains('comptes', `"${userID}"`).then(({ data, error }) => {
        if (error) {
            console.error('Error fetching data from Joueurs:', error.message);
            alert('Failed to fetch data from Joueurs.');
            return;
        }
        if (data && data.length > 0) {
            console.log("data", data);
            const pseudo = data[0].pseudo;
            $('#pseudo').val(pseudo).prop('readonly', false);
            $('#message-joueur').hide();
        } else {
            console.log("data", data);
            $('#registre-preview').hide();
            $('.edit-profil').addClass("no-preview");
            $('#registreForm').hide();
            $('#message-new').hide();
            $('#message-edit').hide();
            $('#message-joueur').show();
        }
    })
        .catch((error) => {
            console.error('Error:', error.message);
            alert('Failed to fetch data.');
        });

    // CHECK SI LE PERSONNAGE EST DANS LE REGISTRE
    supabaseClient.from('Registre').select('*').eq('id', userID).then(({ data: userData, error: userError }) => {
        if (userError) {
            console.error('Error fetching user data:', userError.message);
            return;
        }

        const exists = userData.length > 0;
        if (!exists) {
            console.log("Pas d'entrée de personnage pour cet userID dans le registre :", userID);
            $('#nom').val(userName).prop('readonly', true);
            $('#id').val(userID).prop('readonly', true);
            $('#registre-preview').hide();
            $('.edit-profil').addClass("no-preview");
            $('button#confirmButton span').text("Ajouter mon personnage");
            $('#message-new').show();
            $('#message-edit').hide();
        } else {
            const data = userData[0];
            console.log("data", data);

            $('button#confirmButton span').text("Modifier mes informations");
            $('#message-new').hide();
            $('#message-edit').show();
            $('#registreForm').show();

            // PREVIEW CARD
            $('#registre-preview').show();
            const date = new Date();
            let year = date.getFullYear();
            const dob = data.dob;
            let DOBy = dob.substring(0, 4);
            let age = year - DOBy;
            $('#preview-card').addClass(data.maison);
            $('#profil-lien').attr('href', '/u' + userID);
            $('img#avatar-content').attr('src', data.image);
            $('img#avatar-content').attr('data-src', data.image);
            $('#credits-content').html(data.credits);
            $('#fiche-lien').attr('href', data.fiche);
            $('#jdb-lien').attr('href', data.jdb);
            $('#nom-content').html(data.nom + ', ');
            $('#age').html(age + ' ans');
            $('#location-content').html('<ion-icon name="location-outline"></ion-icon><span>' + data.logement_details + '</span>');
            $('#promo-content').html('<ion-icon name="school-outline"></ion-icon><span>' + data.ecole + ', ' + data.promo + '</span>');

            if (data.occupation1 !== "Étudiant.e" && data.occupation2 !== "Étudiant.e") {
                if (data.occupation1 !== "") {
                    if (data.occupation2 === "" || data.occupation2 === "null" || data.occupation2 === null) {
                        $('#job-content').html('<ion-icon name="briefcase-outline"></ion-icon> <span>' + data.occupation_details1 + '</span>');
                    } else {
                        $('#job-content').html('<ion-icon name="briefcase-outline"></ion-icon> <span>' + data.occupation_details1 + ', ' + data.occupation_details2 + '</span>');
                    }
                } else if (data.occupation2 !== "") {
                    $('#job-content').html('<ion-icon name="briefcase-outline"></ion-icon> <span>' + data.occupation_details2 + '</span>');
                }
            } else if (data.occupation1 === "Étudiant.e" || data.occupation2 === "Étudiant.e") {
                let studentText = "Étudiant.e en ";
                if (data.cursus1 !== "") {
                    studentText += data.cursus1;
                }
                if (data.cursus2 !== "") {
                    studentText += ' et ' + data.cursus2;
                }
                if (data.occupation2 !== "" && data.occupation1 === "Étudiant.e") {
                    studentText += ', ' + data.occupation_details2;
                }
                if (data.occupation1 !== "" && data.occupation2 === "Étudiant.e") {
                    studentText += ', ' + data.occupation_details1;
                }
                $('#job-content').html('<ion-icon name="briefcase-outline"></ion-icon> <span>' + studentText + '</span>');
            } else if ((data.occupation1 === "" || data.occupation1 === null) && (data.occupation2 === "" || data.occupation2 === null) && (data.cursus1 === "" || data.cursus1 === null) && (data.cursus2 === "" || data.cursus2 === null)) {
                $('#job-content').html('<ion-icon name="briefcase-outline"></ion-icon> <span>Sans activité</span>');
            } else if ((data.occupation1 === "Étudiant.e" && data.cursus1 !== "" && (data.cursus2 === "" || data.cursus2 === null)) || (data.occupation2 === "Étudiant.e" && data.cursus2 !== "" && (data.cursus1 === "" || data.cursus1 === null))) {
                let studentText = "Étudiant.e en ";
                if (data.cursus1 !== "") {
                    studentText += data.cursus1;
                }
                if (data.cursus2 !== "") {
                    studentText += data.cursus2;
                }
                $('#job-content').html('<ion-icon name="briefcase-outline"></ion-icon> <span>' + studentText + '</span>');
            }

            if ((data.parti1 === "" || data.parti1 === null || data.parti1 === "null") && (data.parti2 === "" || data.parti2 === null || data.parti2 === "null")) {
                $('#parti-content').html('<ion-icon name="star-outline"></ion-icon> <span>Aucune particularité</span>');
            }
            else if (data.parti1 !== "" && (data.parti2 === "" || data.parti2 === null || data.parti2 === "null")) {
                $('#parti-content').html('<ion-icon name="star-outline"></ion-icon> <span>' + data.parti1 + '</span>');
            }
            else if (data.parti2 !== "" && (data.parti1 === "" || data.parti1 === null || data.parti1 === "null")) {
                $('#parti-content').html('<ion-icon name="star-outline"></ion-icon> <span>' + data.parti2 + '</span>');
            }
            else if (data.parti1 !== "" && data.parti2 !== "") {
                $('#parti-content').html('<ion-icon name="star-outline"></ion-icon> <span>' + data.parti1 + ', ' + data.parti2 + '</span>');
            }

            // FORM PRE-FILL
            $('#nom').val(data.nom).prop('readonly', false);
            $('#id').val(userID).prop('readonly', true);
            $('#type').val(data.type);
            if (data.type === "Pré-lien" || data.type === "Scénario") {
                $('#plLien').show();
                $('#plURL').val(data.plURL).prop('readonly', false);
            } else {
                $('#plLien').hide();
                $('#plURL').val('');
            }
            $('#prenom').val(data.prenom).prop('readonly', false);
            $('#surnom').val(data.surnom).prop('readonly', false);
            $('#ndf').val(data.ndf).prop('readonly', false);
            $('#dob').val(data.dob).prop('readonly', false);
            $('#ryu').prop('checked', data.ryu);
            $('#intouche').prop('checked', data.intouche);
            $('#parti1').val(data.parti1);
            $('#parti2').val(data.parti2);
            if (data.parti1 === "Rōnin" || data.parti2 === "Rōnin") {
                $('#ronin_pouvoir').show();
                $('#ronin_details').val(data.ronin_details).prop('readonly', false);
            } else {
                $('#ronin_pouvoir').hide();
                $('#ronin_details').val('');
            }
            if (data.parti1 === "Henjū" || data.parti2 === "Henjū" || data.parti1 === "Jūjin" || data.parti2 === "Jūjin") {
                $('#animal_forme').show();
                $('#animal').val(data.animal).prop('readonly', false);
            } else {
                $('#animal_forme').hide();
                $('#animal').val('');
            }
            if (data.parti1 === "Mangetsuki" || data.parti2 === "Mangetsuki") {
                $('#mangetsuki_role').show();
                $('#mangetsuki_details').val(data.mangetsuki_details);
            } else {
                $('#mangetsuki_role').hide();
                $('#mangetsuki_details').val('');
            }
            $('#maison').val(data.maison);
            $('#promo').val(data.promo);
            $('#ecole').val(data.ecole);
            if (data.ecole !== "Wakoku" && data.ecole !== "Eiyamachi" && data.ecole !== "Shinmeidai" && data.ecole !== "Chuo-Kōei") {
                $('#otherEcoleInput').show();
                $('#ecole').val("Autre");
                $('#otherEcole').val(data.ecole);
            } else {
                $('#otherEcoleInput').hide();
                $('#otherEcole').val('');
            }
            $('#occupation1').val(data.occupation1);
            $('#occupation_lieu1').val(data.occupation_lieu1);
            if (data.occupation1 !== "" && data.occupation1 !== null) {
                $('#occup1_details').show();
                $('#occupation_details1').val(data.occupation_details1);
            } else {
                $('#occup1_details').hide();
                $('#occupation_details1').val('');
            }
            $('#occupation2').val(data.occupation2);
            $('#occupation_lieu2').val(data.occupation_lieu2);
            if (data.occupation2 !== "" && data.occupation2 !== null) {
                $('#occup2_details').show();
                $('#occupation_details2').val(data.occupation_details2);
            } else {
                $('#occup2_details').hide();
                $('#occupation_details2').val('');
            }
            if (data.dustquash === true) {
                $('#dustquash').val("TRUE");
            } else {
                $('#dustquash').val("FALSE");
            }
            if (data.occupation1 === "Étudiant.e" || data.occupation2 === "Étudiant.e") {
                $('.formsection.etudiant').show();
                $('#cursus1').val(data.cursus1);
                $('#cursus1_annee').val(data.cursus1_annee);
                if (data.cursus2 === "" || data.cursus2 === null) {
                    $('#cursus2').val();
                } else {
                    $('#cursus2').val(data.cursus2);
                }
                $('#cursus2_annee').val(data.cursus2_annee);
                $('#option1').val(data.option1);
                $('#option2').val(data.option2);
                if (data.cosmoball === true) {
                    $('#cosmoball').val("TRUE");
                } else {
                    $('#cosmoball').val("FALSE");
                }
                $('#cosmo_equipe').val(data.cosmo_equipe);
                $('#cosmo_poste').val(data.cosmo_poste);
                $('#cosmo_cap').prop('checked', data.cosmo_cap);
            } else {
                $('.formsection.etudiant').hide();
            }
            $('#logement').val(data.logement);
            $('#logement_details').val(data.logement_details);
            $('#logement_type').val(data.logement_type);
            $('#coloc').val(data.coloc).prop('readonly', false);
            $('#faceclaim').val(data.faceclaim).prop('readonly', false);
            $('#image').val(data.image).prop('readonly', false);
            $('#credits').val(data.credits).prop('readonly', false);
            $('#fiche').val(data.fiche).prop('readonly', false);
            $('#jdb').val(data.jdb).prop('readonly', false);
        }
    })
        .catch(error => {
            console.error('Error:', error.message);
            alert('Failed to fetch user data.');
        });


    $('#registreForm').submit(async function (e) {
        e.preventDefault();

        $('#spinner').removeClass('hidden');
        $('#confirmButton').prop('disabled', true);

        try {
            const userID = _userdata.user_id;
            // const userID = 3;

            // Fetch form data
            const nom = $('#nom').val();
            const type = $('#type').val();
            const plURL = $('#plURL').val();
            const prenom = $('#prenom').val();
            const surnom = $('#surnom').val() || null;
            const ndf = $('#ndf').val();
            const dob = $('#dob').val();
            const ryu = $('#ryu').prop('checked');
            const intouche = $('#intouche').prop('checked');
            const parti1 = $('#parti1').val() || null;
            const parti2 = $('#parti2').val() || null;
            const ronin_details = $('#ronin_details').val() || null;
            const animal = $('#animal').val() || null;
            const mangetsuki_details = $('#mangetsuki_details').val() || null;
            const maison = $('#maison').val();
            const promo = $('#promo').val();
            const ecole = $('#ecole').val();
            const otherEcole = $('#otherEcole').val() || null;
            const occupation1 = $('#occupation1').val();
            const occupation_lieu1 = $('#occupation_lieu1').val() || null;
            const occupation_details1 = $('#occupation_details1').val() || null;
            const occupation2 = $('#occupation2').val() || null;
            const occupation_lieu2 = $('#occupation_lieu2').val() || null;
            const occupation_details2 = $('#occupation_details2').val() || null;
            const dustquash = $('#dustquash').prop('checked');
            const cursus1 = $('#cursus1').val() || null;
            const cursus1_annee = $('#cursus1_annee').val() === '' ? null : $('#cursus1_annee').val();
            const cursus2 = $('#cursus2').val() || null;
            const cursus2_annee = $('#cursus2_annee').val() === '' ? null : $('#cursus2_annee').val();
            const option1 = $('#option1').val() || null;
            const option2 = $('#option2').val() || null;
            const cosmoball = $('#cosmoball').prop('checked');
            const cosmo_equipe = $('#cosmo_equipe').val() || null;
            const cosmo_poste = $('#cosmo_poste').val() || null;
            const cosmo_cap = $('#cosmo_cap').prop('checked');
            const logement = $('#logement').val();
            const logement_details = $('#logement_details').val();
            const logement_type = $('#logement_type').val();
            const colocContent = $('#coloc').val() || null;
            let coloc = colocContent 
                ? colocContent.replace(/\n/g, '<br />')
                            .replace(/\[b\]/g, '<strong>')
                            .replace(/\[\/b\]/g, '</strong>')
                            .replace(/\[i\]/g, '<i>')
                            .replace(/\[\/i\]/g, '</i>')
                : ''; // If colocContent is null or undefined, set coloc to an empty string

            const pseudo = $('#pseudo').val();
            const faceclaim = $('#faceclaim').val();
            const image = $('#image').val();
            const credits = $('#credits').val();
            const fiche = $('#fiche').val();
            const jdb = $('#jdb').val();

            // Form data for Registre
            const formData1 = {
                id: userID,
                nom, type, plURL, prenom, surnom, ndf, dob, ryu, intouche,
                parti1, parti2, ronin_details, animal, mangetsuki_details,
                maison, promo, ecole, otherEcole,
                occupation1, occupation_lieu1, occupation_details1,
                occupation2, occupation_lieu2, occupation_details2,
                dustquash,
                cursus1, cursus1_annee, cursus2, cursus2_annee, option1, option2,
                cosmoball, cosmo_equipe, cosmo_poste, cosmo_cap,
                logement, logement_details, logement_type, coloc,
                pseudo, faceclaim, image, credits, fiche, jdb
            };

            // Form data for Joueurs
            const formData2 = {
                pseudo
            };

            const { data: userData, error: userError } = await supabaseClient
                .from('Registre')
                .select('*')
                .eq('id', userID);
            if (userError) {
                console.error('Error fetching user data from Registre:', userError.message);
                alert('Failed to fetch user data from Registre.');
                return;
            }

            // BEGINNING

            const existsInRegistre = userData.length > 0;

            if (!existsInRegistre) {
                // Insert new entry in Registre
                const { data: insertData, error: insertError } = await supabaseClient
                    .from('Registre')
                    .insert([formData1]);
                
                if (insertError) throw insertError;

                // Fetch Joueurs data based on the pseudo
                const { data: joueursData, error: joueursError } = await supabaseClient
                    .from('Joueurs')
                    .select('*')
                    .eq('pseudo', formData2.pseudo);
                    
                // Fetch Joueurs data based on the userID
                const { data: joueursByID, error: joueursByIDError } = await supabaseClient
                    .from('Joueurs')
                    .select('*')
                    .contains('comptes', `"${userID}"`);

                // Check if pseudo or userID exists in Joueurs
                const pseudoExistsInJoueurs = joueursData.length > 0;

                if (pseudoExistsInJoueurs) {
                    // Handle the situation where pseudo and userID already exist in Joueurs
                    const joueursRow = joueursData[0];
                    
                    // Ensure comptes contains strings and check for userID
                    if (!joueursRow.comptes.includes(String(userID))) {
                        joueursRow.comptes.push(String(userID)); // Push userID as a string
                        // Update Joueurs table with the updated comptes array
                        const { error: updateError2 } = await supabaseClient
                            .from('Joueurs')
                            .update({ comptes: joueursRow.comptes })
                            .eq('pseudo', formData2.pseudo);
                        
                        if (updateError2) throw updateError2;
                    }
                } else if (joueursByID.length > 0) {
                    // Ensure IDs in comptes are stored as strings
                    const comptesArray = joueursByID[0].comptes.map(id => String(id)); 

                    console.log("User ID to check:", userID); // Check userID value
                    console.log("Current comptes array:", comptesArray); // Check comptes array value

                    // Check if userID is already in the comptes array
                    if (!comptesArray.includes(String(userID))) { // Ensure userID is a string for comparison
                        comptesArray.push(String(userID)); // Add userID as a string

                        // Update pseudo in the Joueurs table
                        const { error: updateError2 } = await supabaseClient
                            .from('Joueurs')
                            .update({ pseudo: formData2.pseudo, comptes: comptesArray }) // Update with new comptes array
                            .eq('id', joueursByID[0].id);
                        
                        if (updateError2) throw updateError2;

                        // Update pseudo in Registre for each ID in comptesArray
                        for (const id of comptesArray) { // Use updated comptesArray
                            const { error: updateRegistreError } = await supabaseClient
                                .from('Registre')
                                .update({ pseudo: formData2.pseudo })
                                .eq('id', id);
                            
                            if (updateRegistreError) {
                                console.error(`Error updating pseudo for ID ${id} in Registre:`, updateRegistreError.message);
                                alert(`Failed to update pseudo for ID ${id} in Registre.`);
                            }
                        }
                    } else {
                        console.log(`User ID ${userID} is already in the comptes array. No changes made.`);
                    }
                } else {
                    // Create new Joueurs entry since userID doesn't exist in Registre
                    const { data: insertJoueursData, error: insertJoueursError } = await supabaseClient
                        .from('Joueurs')
                        .insert([{
                            pseudo: formData2.pseudo,
                            mainCompte: String(userID), // Ensure mainCompte is a string
                            comptes: [String(userID)], // Ensure comptes contains the userID as a string
                        }]);
                    
                    if (insertJoueursError) throw insertJoueursError;
                }

                location.reload();
                removeQueryParameters();
                alert('Nouveau personnage ajouté !');

            } else {
                // Update existing data in Registre
                const { error: updateError1 } = await supabaseClient
                    .from('Registre')
                    .update(formData1)
                    .eq('id', userID);
                
                if (updateError1) throw updateError1;

                // Fetch Joueurs data based on the pseudo
                const { data: joueursData, error: joueursError } = await supabaseClient
                    .from('Joueurs')
                    .select('*')
                    .eq('pseudo', formData2.pseudo);

                // Fetch Joueurs data based on the userID
                const { data: joueursByID, error: joueursByIDError } = await supabaseClient
                    .from('Joueurs')
                    .select('*')
                    .contains('comptes', `"${userID}"`);

                // Check if pseudo or userID exists in Joueurs
                const pseudoExistsInJoueurs = joueursData.length > 0;
                
                console.log("joueursData", joueursData);
                console.log("pseudoExistsInJoueurs", pseudoExistsInJoueurs);
                console.log("joueursByID", joueursByID);

                if (pseudoExistsInJoueurs) {
                    // Handle the situation where pseudo and userID already exist in Joueurs
                    const joueursRow = joueursData[0];
                    
                    // Ensure comptes contains strings and check for userID
                    if (!joueursRow.comptes.includes(String(userID))) {
                        joueursRow.comptes.push(String(userID)); // Push userID as a string
                        // Update Joueurs table with the updated comptes array
                        const { error: updateError2 } = await supabaseClient
                            .from('Joueurs')
                            .update({ comptes: joueursRow.comptes })
                            .eq('pseudo', formData2.pseudo);
                        
                        if (updateError2) throw updateError2;
                    }
                } else if (joueursByID.length > 0) {
                    // Ensure IDs in comptes are stored as strings
                    const comptesArray = joueursByID[0].comptes.map(id => String(id)); 

                    console.log("User ID to check:", userID); // Check userID value
                    console.log("Current comptes array:", comptesArray); // Check comptes array value

                    // Check if userID is already in the comptes array
                    if (!comptesArray.includes(String(userID))) { // Ensure userID is a string for comparison
                        comptesArray.push(String(userID)); // Add userID as a string

                        // Update pseudo in the Joueurs table
                        const { error: updateError2 } = await supabaseClient
                            .from('Joueurs')
                            .update({ pseudo: formData2.pseudo, comptes: comptesArray }) // Update with new comptes array
                            .eq('id', joueursByID[0].id);
                        
                        if (updateError2) throw updateError2;

                        // Update pseudo in Registre for each ID in comptesArray
                        for (const id of comptesArray) { // Use updated comptesArray
                            const { error: updateRegistreError } = await supabaseClient
                                .from('Registre')
                                .update({ pseudo: formData2.pseudo })
                                .eq('id', id);
                            
                            if (updateRegistreError) {
                                console.error(`Error updating pseudo for ID ${id} in Registre:`, updateRegistreError.message);
                                alert(`Failed to update pseudo for ID ${id} in Registre.`);
                            }
                        }
                    } else {
                        console.log(`User ID ${userID} is already in the comptes array. No changes made.`);
                    }
                } else {
                    // Create new Joueurs entry since userID doesn't exist in Registre
                    const { data: insertJoueursData, error: insertJoueursError } = await supabaseClient
                        .from('Joueurs')
                        .insert([{
                            pseudo: formData2.pseudo,
                            mainCompte: String(userID), // Ensure mainCompte is a string
                            comptes: [String(userID)], // Ensure comptes contains the userID as a string
                        }]);
                    
                    if (insertJoueursError) throw insertJoueursError;
                }

                location.reload();
                removeQueryParameters();
                alert('Tes informations ont été mises à jour !');
            }

            // END
        } catch (error) {
            console.error('Error:', error.message);
            alert('Une erreur est survenue. Veuillez réessayer.');
        } finally {
            $('#spinner').addClass('hidden');
            $('#confirmButton').prop('disabled', false);
        }
    });

});

// REMPLISSAGE INITIAL DE LA PREVIEW
$(document).ready(function() {
    function updatePreview() {
        const userID = _userdata.user_id;
        const nom = $('#nom').val();

        const date = new Date();
        let year = date.getFullYear();
        const dob = $('#dob').val();
        let DOBy = dob.substring(0, 4);
        let age = year - DOBy;

        const parti1 = $('#parti1').val();
        const parti2 = $('#parti2').val();

        const maison = $('#maison').val();
        const promo = $('#promo').val();
        const ecole = $('#ecole').val();
        const otherEcole = $('#otherEcole').val() || "Autre école ici";

        const occupation1 = $('#occupation1').val().trim(); 
        const occupation_details1 = $('#occupation_details1').val().trim();
        const occupation2 = $('#occupation2').val().trim();
        const occupation_details2 = $('#occupation_details2').val().trim();

        const cursus1 = $('#cursus1').val().trim();
        const cursus2 = $('#cursus2').val().trim();

        const logement = $('#logement').val();
        const logement_details = $('#logement_details').val();

        const image = $('#image').val();
        const credits = $('#credits').val();
        const fiche = $('#fiche').val();
        const jdb = $('#jdb').val();

        // Updating preview elements
        $('#preview-card').addClass(maison);
        $('#profil-lien').attr('href', '/u' + userID);
        $('img#avatar-content').attr('src', image);
        $('img#avatar-content').attr('data-src', image);
        $('#credits-content').html(credits);
        $('#fiche-lien').attr('href', fiche);
        $('#jdb-lien').attr('href', jdb);
        $('#nom-content').html(nom + ', ');
        $('#age').html(age + ' ans');
        $('#location-content').html('<ion-icon name="location-outline"></ion-icon><span>' + logement_details + '</span>');

        // Handle "promo-content"
        if (ecole === "Autre") {
            $('#promo-content').html('<ion-icon name="school-outline"></ion-icon><span>' + otherEcole + ', ' + promo + '</span>');
        } else {
            $('#promo-content').html('<ion-icon name="school-outline"></ion-icon><span>' + ecole + ', ' + promo + '</span>');
        }

        // Handle empty occupation fields explicitly
        if (occupation1 === "") {
            $('#occupation_details1').val(""); 
        }
        if (occupation2 === "") {
            $('#occupation_details2').val(""); 
        }

        // Logic for displaying job content
        if (occupation1 !== "Étudiant.e" && occupation2 !== "Étudiant.e") {
            if (occupation1 !== "") {
                if (occupation2 !== "") {
                    $('#job-content').html('<ion-icon name="briefcase-outline"></ion-icon> <span>' + occupation_details1 + ', ' + occupation_details2 + '</span>');
                } else {
                    $('#job-content').html('<ion-icon name="briefcase-outline"></ion-icon> <span>' + occupation_details1 + '</span>');
                }
            } else if (occupation2 !== "") {
                $('#job-content').html('<ion-icon name="briefcase-outline"></ion-icon> <span>' + occupation_details2 + '</span>');
            }
        } else if (occupation1 === "Étudiant.e" || occupation2 === "Étudiant.e") {
            let studentText = "Étudiant.e en ";
            if (cursus1 !== "") {
                studentText += cursus1;
            }
            if (cursus2 !== "") {
                studentText += ' et ' + cursus2;
            }
            if (occupation2 !== "" && occupation1 === "Étudiant.e") {
                studentText += ', ' + occupation_details2;
            }
            if (occupation1 !== "" && occupation2 === "Étudiant.e") {
                studentText += ', ' + occupation_details1;
            }
            $('#job-content').html('<ion-icon name="briefcase-outline"></ion-icon> <span>' + studentText + '</span>');
        } else if (occupation1 === "" && occupation2 === "" && cursus1 === "" && cursus2 === "") {
            $('#job-content').html('<ion-icon name="briefcase-outline"></ion-icon> <span>Sans activité</span>');
        } else if ((occupation1 === "Étudiant.e" && cursus1 !== "" && cursus2 === "") || (occupation2 === "Étudiant.e" && cursus2 !== "" && cursus1 === "")) {
            let studentText = "Étudiant.e en ";
            if (cursus1 !== "") {
                studentText += cursus1;
            }
            if (cursus2 !== "") {
                studentText += cursus2;
            }
            $('#job-content').html('<ion-icon name="briefcase-outline"></ion-icon> <span>' + studentText + '</span>');
        }

        // Handling "parti-content"
        if ((parti1 === "" || parti1 === null || parti1 === "Aucune") && (parti2 === "" || parti2 === null || parti2 === "Aucune")) {
            $('#parti-content').html('<ion-icon name="star-outline"></ion-icon> <span>Aucune particularité</span>');
        } else if (parti1 !== "" && parti2 !== "") {
            $('#parti-content').html('<ion-icon name="star-outline"></ion-icon> <span>' + parti1 + ', ' + parti2 + '</span>');
        } else if (parti1 !== "" && (parti2 === "" || parti2 === null || parti2 === "Aucune")) {
            $('#parti-content').html('<ion-icon name="star-outline"></ion-icon> <span>' + parti1 + '</span>');
        } else if (parti2 !== "" && (parti1 === "" || parti1 === null || parti1 === "Aucune")) {
            $('#parti-content').html('<ion-icon name="star-outline"></ion-icon> <span>' + parti2 + '</span>');
        }
    }

    // Run the updatePreview function on page load
    $(window).on('load', function() {
        updatePreview(); // Ensure the preview is updated after everything is fully loaded
    });

    // Listen for input changes and run the function again
    $('#registreForm').on('input', function () {
        updatePreview();
    });

    // Handle scroll position for fixed div
    const div = $('#registre-preview');
    const fixedPoint = 56;
    function handleScroll() {
        const scrollTop = $(window).scrollTop();
        if (scrollTop >= fixedPoint) {
            div.addClass('fixed');
        } else {
            div.removeClass('fixed');
        }
    }
    $(window).on('scroll', handleScroll);
});