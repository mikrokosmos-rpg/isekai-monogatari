window.onload = function () {
  if (window.location.href.includes("t2279-wip-listings-metiers-logements-et-particularites")) {
    // Define the URL of the external JSON file
    const jsonFileURL = 'https://mikrokosmos-rpg.gitlab.io/isekai-monogatari/imo-directory/imo_personnages.json';
    const cursusDataList = document.getElementById('cursus-grid'); // Separate container for cursus
    const optionsDataList = document.getElementById('options-grid'); // Separate container for options
    const magieElementaireDataList = document.getElementById('magie-elementaire-grid'); // Separate container for Magie Élémentaire
    const cosmoballDataList = document.getElementById('cosmo-grid'); // Separate container for Cosmoball

    fetch(jsonFileURL)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        // Define objects to store unique values for cursus and option
        const uniqueCursusValues = {};
        const uniqueOptionValues = {};
        const magieElementaireNames = {};

        data.forEach(item => {
          for (let i = 1; i <= 2; i++) {
            const cursus = item[`cursus${i}`];
            const option = item[`option${i}`];
            const cursusAnnee = item[`cursus${i}_annee`];

            // Check if cursus exists and is not empty
            if (cursus && cursus.trim() !== "") {
              // Check if the cursus value already exists in the uniqueCursusValues object
              if (!uniqueCursusValues[cursus]) {
                // If it doesn't exist, create a new entry with an empty array
                uniqueCursusValues[cursus] = [];
              }
              // Push the name into the corresponding cursus key along with the cursus1_annee or cursus2_annee
              if (cursusAnnee) {
                uniqueCursusValues[cursus].push({
                  nom: item.nom,
                  profilID: item.profilID,
                  annee: cursusAnnee.replace(/(ème année|ère année)/g, 'e')
                });
              } else {
                uniqueCursusValues[cursus].push({
                  nom: item.nom,
                  profilID: item.profilID
                });
              }
            }

            // Check if option exists and is not empty
            if (option && option.trim() !== "") {
              // Group options containing "Magie Élémentaire"
              if (option.includes("Magie Élémentaire")) {
                // Extract elements between parentheses
                const elements = option.match(/\(([^)]+)\)/);
                if (elements && elements.length > 1) {
                  const magieElements = elements[1].split(' ');
                  const name = `${item.nom}`;
                  if (!magieElementaireNames[name]) {
                    magieElementaireNames[name] = {
                      elements: magieElements,
                      profilID: item.profilID // Store profilID
                    };
                  } else {
                    magieElementaireNames[name].elements.push(...magieElements); // Append Magie Élémentaire elements
                  }
                }
              } else {
                // Check if the option value already exists in the uniqueOptionValues object
                if (!uniqueOptionValues[option]) {
                  // If it doesn't exist, create a new entry with an empty array
                  uniqueOptionValues[option] = {
                    names: [],
                    magieElements: [] // Store Magie Élémentaire elements here
                  };
                }
                // Push the name into the corresponding option key
                uniqueOptionValues[option].names.push({
                  nom: item.nom,
                  profilID: item.profilID
                });

                // Extract Magie Élémentaire elements and store them
                if (option.includes("Magie Élémentaire")) {
                  const elements = option.match(/\(([^)]+)\)/);
                  if (elements && elements.length > 1) {
                    const magieElements = elements[1].split(' ');
                    uniqueOptionValues[option].magieElements.push(...magieElements);
                  }
                }
              }
            }
          }
        });

        // Function to create and append the etudiant-name structure
        function createEtudiantNameStructure(item, magieElements = []) {
          const etudiantNameSpan = document.createElement('span');
          etudiantNameSpan.classList.add('etudiant-name');

          // Create the link
          const link = document.createElement('a');
          link.href = `/u${item.profilID}`;
          link.textContent = `@${item.nom}`; // Include "@" before name
          link.target = '_blank';

          etudiantNameSpan.appendChild(link);

          // Add annee span if it exists
          if (item.annee) {
            const anneeSpan = document.createElement('span');
            anneeSpan.classList.add('tag');
            anneeSpan.textContent = item.annee;
            etudiantNameSpan.appendChild(anneeSpan);
          }

          // Iterate through magie elements and create tags (if applicable)
          magieElements.forEach(magieElement => {
            const tag = document.createElement('span');
            tag.classList.add('tag');
            tag.textContent = magieElement;
            etudiantNameSpan.appendChild(tag);
          });

          return etudiantNameSpan;
        }

        // Display unique cursus values
        for (const cursusValue in uniqueCursusValues) {
          if (uniqueCursusValues.hasOwnProperty(cursusValue)) {
            
            // Define the mapping of cursusValue to text content
            const cursusTitleMapping = {
              MAE: "Maître Alchimiste et Enchanteur (MAE)",
              ASPC: "Apprentissage Physique des Sports Cosmiques (ASPC)",
              Archéomagie: "Archéomagie : Études des Vestiges",
              EFFM: "Études de la Faune et de la Flore Magique",
              MACA: "Maître des Arts Catalyseurs et des Artefacts (MACA)",
              SNEP: "Sciences Naturelles et de l'Environnement Poussiéreux (SNEP)",
              Psychomagie: "Psychomagie",
              Médicomagie: "Médicomagie",
              Technomagie: "Technomagie",
              Architecture: "Architecture Magique",
              "Arts Visuels": "Arts Visuels",
              Cinéma: "Cinéma et Techniques Audiovisuelles",
              Danse: "Danse",
              Musique: "Musique et Chant",
              Mode: "Mode",
              Dramaturgie: "Dramaturgie",
              Histoire: "Histoire des Mondes",
              Langues: "Langues et Civilisations",
              "Sciences Humaines": "Sciences Humaines et Sociales",
              Journalisme: "Journalisme",
              Droit: "Droit et Sciences Politicomagiques",
              Économie: "Économie et Gestion",
              "Bataillon d'Exploration": "Bataillon d'Exploration",
              "Brigades Spéciales": "Brigades Spéciales",
              Sentinelle: "Sentinelle",
            };
                                
            const listItem = document.createElement('div');
            listItem.classList.add('etudiants-item');
            
            const cursustitleSpan = document.createElement('span');
            cursustitleSpan.classList.add('cursus-title');
	    if (cursusTitleMapping[cursusValue]) {
              cursustitleSpan.textContent = cursusTitleMapping[cursusValue];
            }

            const cursusSpan = document.createElement('span');
            cursusSpan.classList.add('cursus');
            cursusSpan.textContent = cursusValue;
	
            const namesSpan = document.createElement('span');
            namesSpan.classList.add('names');

            // Iterate through the names in the current category and create etudiant-name structure
            uniqueCursusValues[cursusValue].forEach(item => {
              const etudiantNameSpan = createEtudiantNameStructure(item);
              namesSpan.appendChild(etudiantNameSpan);
            });

            listItem.appendChild(cursustitleSpan);
            listItem.appendChild(cursusSpan);
            listItem.appendChild(namesSpan);

            cursusDataList.appendChild(listItem); // Add to cursus container
          }
        }

        // Display unique option values
        for (const optionValue in uniqueOptionValues) {
          if (uniqueOptionValues.hasOwnProperty(optionValue)) {
            const listItem = document.createElement('div');
            listItem.classList.add('etudiants-item'); // Change to your desired class name

            const optionSpan = document.createElement('span');
            optionSpan.classList.add('option');
            optionSpan.textContent = optionValue;

            const namesSpan = document.createElement('span');
            namesSpan.classList.add('names');

            // Iterate through the names in the current category and create etudiant-name structure
            uniqueOptionValues[optionValue].names.forEach(item => {
              const etudiantNameSpan = createEtudiantNameStructure(item, uniqueOptionValues[optionValue].magieElements);
              namesSpan.appendChild(etudiantNameSpan);
            });

            listItem.appendChild(optionSpan);
            listItem.appendChild(namesSpan);

            optionsDataList.appendChild(listItem); // Add to options container
          }
        }

        // Display "Magie Élémentaire" group with small tags
        const listItem = document.createElement('div');
        listItem.classList.add('etudiants-item'); // Change to your desired class name

        const cursusSpan = document.createElement('span');
        cursusSpan.classList.add('option');
        cursusSpan.textContent = 'Magie Élémentaire';

        const namesSpan = document.createElement('span');
        namesSpan.classList.add('names');

        // Iterate through "Magie Élémentaire" names and their associated elements
        for (const name in magieElementaireNames) {
          if (magieElementaireNames.hasOwnProperty(name)) {
            const etudiantNameSpan = createEtudiantNameStructure(
              { nom: name, profilID: magieElementaireNames[name].profilID },
              magieElementaireNames[name].elements
            );
            namesSpan.appendChild(etudiantNameSpan);
          }
        }

        listItem.appendChild(cursusSpan);
        listItem.appendChild(namesSpan);

        magieElementaireDataList.appendChild(listItem); // Add to Magie Élémentaire container
          
      })
      .catch(error => {
        console.error('Error fetching JSON:', error);
      });



   setTimeout(function () {
     		    var qsRegex;
                    var $cursusGrid = $('.cursus-grid').isotope({
                        itemSelector: '.etudiants-item',
                        stagger: 30,
                        layoutMode: 'vertical',
                        filter: function() {
                          return qsRegex ? $(this).text().match( qsRegex ) : true;
                        },
                        getSortData: {
                          cursus: '.cursus', // Sort by cursus
                          name: '.etudiant-name a',
                          annee: '.etudiant-name .tag', // Sort by name within each cursus
                        },
                        sortBy: ['cursus', 'name', 'annee'], // Initial sorting by cursus and then by name
                      });
                  
                      var $optionsGrid = $('.options-grid').isotope({
                        itemSelector: '.etudiants-item',
                        stagger: 30,
                        layoutMode: 'vertical',
                        getSortData: {
                          option: '.option', // Sort by option
                          name: '.etudiant-name a', // Sort by name within each option
                        },
                        sortBy: ['option', 'name'], // Initial sorting by option and then by name
                      });
                  
                      var $magieElementaireGrid = $('.magie-elementaire-grid').isotope({
                          itemSelector: '.etudiants-item',
                          stagger: 30,
                          layoutMode: 'vertical',
                          getSortData: {
                            name: '.etudiant-name a', // Sort by name within "Magie Élémentaire"
                            elements: '.etudiant-name .tag', // Sort elements tags within "Magie Élémentaire"
                          },
                          sortBy: ['name', 'elements'], // Initial sorting by name and then elements
                        });
     
     			// use value of search field to filter
                        var $quicksearch = $('.quicksearch.cursus').keyup( debounce( function() {
                            // qsRegex = new RegExp('^' + $quicksearch.val(), 'i');
                            qsRegex = new RegExp( $quicksearch.val(), 'gi' );
                            $cursusGrid.isotope();
                          }, 200 ) );
                
                        // debounce so filtering doesn't happen every millisecond
                        function debounce( fn, threshold ) {
                          var timeout;
                          threshold = threshold || 100;
                          return function debounced() {
                            clearTimeout( timeout );
                            var args = arguments;
                            var _this = this;
                            function delayed() {
                              fn.apply( _this, args );
                            }
                            timeout = setTimeout( delayed, threshold );
                          };
                        }
                    
                        // Add click event listeners for sorting
                        $('#sort-by-cursus').on('click', function () {
                          $cursusGrid.isotope({ sortBy: ['cursus', 'name'] });
                        });
                    
                        $('#sort-by-option').on('click', function () {
                          $optionsGrid.isotope({ sortBy: ['option', 'name'] });
                        });
                    
                        $('#sort-by-magie-elementaire').on('click', function () {
                          $magieElementaireGrid.isotope({ sortBy: ['name', 'elements'] });
                        });
    }, 1000);


  }
};
